$(document).ready(function() {
  /**
   * Cierra automáticamente los mensajes informativos
   */
  setTimeout(function() {
      $(".alert.alert-info").slideUp(1500, 0, function() {
          $(this).remove();
      });
  }, 4000);
});
