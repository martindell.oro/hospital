$(document).ready(function() {
    $('#reservas_respirador').closest('div').hide();
    $('.datepicker').datepicker({
      language: 'es',
      orientation: 'auto',
      useCurrent: false,
      autoclose: true,
      disableTouchKeyboard: true,
      readonly: true
    });

    $.getJSON("/reservas/ocupacion.json", function(data) {
      $("#reservas_hospedaje").html('');
      $("#reservas_hospedaje").append('<option selected="true" disabled="disabled">Seleccione una opción</option>');
      $.each(data, function(key, val){
        const institucion = (key == 'institucionHospitalaria') ? 'Institución Hospitalaria' : 'Institución Extrahospitalaria ';
        $("#reservas_hospedaje").append(`<optgroup label="${institucion}">`);
          $.each(val, function(){
            const respiradores = (this.respiradoresDisponibles > 0 ) ? this.respiradoresDisponibles + ' Respiradores Disponibles' : '';
            const disabled = (this.disponible == 0) ? 'disabled' : '';
            $("#reservas_hospedaje").append('<option '+disabled+' value="'+ this.hospedaje+'"'+' data-respirador='+this.respiradoresDisponibles+'>'+ this.nombre +' ('+this.disponible+') '+
            respiradores +'</option>');
          });
        $("#reservas_hospedaje").append('</optgroup>');
      });
      $('#reservas_hospedaje').on('change', function() {
        if ($(this).find(":selected").data('respirador')) {
          return $('#reservas_respirador').closest('div').show();
        }
        return $('#reservas_respirador').closest('div').hide();
      });
    });
    $("form[name=reservas]").submit(function () {
        $("#btnAsociar").attr("disabled", true);
        return true;
    });
});
