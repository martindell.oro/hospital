$(document).ready(function() {
    $('.datepicker').datepicker({
      language: 'es',
      orientation: 'auto',
      useCurrent: false,
      autoclose: true,
      disableTouchKeyboard: true,
      readonly: true
    });
    $("form[name=enfermeria_guardia]").submit(function () {
        $("#btnAsociar").attr("disabled", true);
        return true;
    });
});
