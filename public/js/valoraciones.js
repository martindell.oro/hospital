$(document).ready(function() {
  const date = new Date();
  date.setDate(date.getDate());
    $('.datepicker').datepicker({
      language: 'es',
      orientation: 'auto',
      useCurrent: false,
      autoclose: true,
      disableTouchKeyboard: true,
      readonly: true
    });
    $('#valoraciones_fechaIngreso').val(date.toLocaleDateString('es'));
    if(!$('#valoraciones_fechaAplicacionAntigripal').val()){
      $('#valoraciones_fechaAplicacionAntigripal').closest('.form-group').hide();
    }
    $('#valoraciones_vacunas').on('change', function() {
      const checks = $(this).find(":checked").text();
      if ( checks.indexOf('Antigripal')>0) {
        return $('#valoraciones_fechaAplicacionAntigripal').closest('.form-group').show();
      }
      return $('#valoraciones_fechaAplicacionAntigripal').closest('.form-group').hide();
    });
    $("form[name=valoraciones]").submit(function () {
        $(".btn").attr("disabled", true);
        return true;
    });

});
