<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SeguimientoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Seguimiento
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellido;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provincia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaArribo;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $procedencia;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $finalizoSeguimiento;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SeguimientoObservaciones", mappedBy="seguimiento")
     */
    private $seguimientoObservaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->seguimientoObservaciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(?\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(?string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(?string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getDomicilio(): ?string
    {
        return $this->domicilio;
    }

    public function setDomicilio(?string $domicilio): self
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getFechaArribo(): ?\DateTimeInterface
    {
        return $this->fechaArribo;
    }

    public function setFechaArribo(?\DateTimeInterface $fechaArribo): self
    {
        $this->fechaArribo = $fechaArribo;

        return $this;
    }

    public function getProcedencia(): ?string
    {
        return $this->procedencia;
    }

    public function setProcedencia(?string $procedencia): self
    {
        $this->procedencia = $procedencia;

        return $this;
    }

    public function getFinalizoSeguimiento(): ?bool
    {
        return $this->finalizoSeguimiento;
    }

    public function setFinalizoSeguimiento(?bool $finalizoSeguimiento): self
    {
        $this->finalizoSeguimiento = $finalizoSeguimiento;

        return $this;
    }

    /**
     * @return Collection|SeguimientoObservaciones[]
     */
    public function getSeguimientoObservaciones(): Collection
    {
        return $this->seguimientoObservaciones;
    }

    public function addSeguimientoObservacione(SeguimientoObservaciones $seguimientoObservacione): self
    {
        if (!$this->seguimientoObservaciones->contains($seguimientoObservacione)) {
            $this->seguimientoObservaciones[] = $seguimientoObservacione;
            $seguimientoObservacione->setSeguimiento($this);
        }

        return $this;
    }

    public function removeSeguimientoObservacione(SeguimientoObservaciones $seguimientoObservacione): self
    {
        if ($this->seguimientoObservaciones->contains($seguimientoObservacione)) {
            $this->seguimientoObservaciones->removeElement($seguimientoObservacione);
            // set the owning side to null (unless already changed)
            if ($seguimientoObservacione->getSeguimiento() === $this) {
                $seguimientoObservacione->setSeguimiento(null);
            }
        }

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
      $this->createdAt = new \DateTime("now");
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
      $this->updatedAt = new \DateTime("now");
    }
}
