<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CuadrosClinicosRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CuadrosClinicos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $servicio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cama;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sala;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pacientes", inversedBy="cuadrosClinicos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $paciente;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CuadrosClinicosItem", mappedBy="cuadrosClinicos")
     */
    private $cuadrosClinicosItems;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServicio(): ?string
    {
        return $this->servicio;
    }

    public function setServicio(?string $servicio): self
    {
        $this->servicio = $servicio;

        return $this;
    }

    public function getCama(): ?string
    {
        return $this->cama;
    }

    public function setCama(?string $cama): self
    {
        $this->cama = $cama;

        return $this;
    }

    public function getSala(): ?string
    {
        return $this->sala;
    }

    public function setSala(?string $sala): self
    {
        $this->sala = $sala;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPaciente(): ?Pacientes
    {
        return $this->paciente;
    }

    public function setPaciente(?Pacientes $paciente): self
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
      $this->createdAt = new \DateTime("now");
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * @return Collection|CuadrosClinicosItem[]
     */
    public function getCuadrosClinicosItems(): Collection
    {
        return $this->cuadrosClinicosItems;
    }

    public function addCuadrosClinicosItem(CuadrosClinicosItem $cuadrosClinicosItem): self
    {
        if (!$this->cuadrosClinicosItems->contains($cuadrosClinicosItem)) {
            $this->cuadrosClinicosItems[] = $cuadrosClinicosItem;
            $cuadrosClinicosItem->setCuadrosClinicos($this);
        }

        return $this;
    }

    public function removeCuadrosClinicosItem(CuadrosClinicosItem $cuadrosClinicosItem): self
    {
        if ($this->cuadrosClinicosItems->contains($cuadrosClinicosItem)) {
            $this->cuadrosClinicosItems->removeElement($cuadrosClinicosItem);
            // set the owning side to null (unless already changed)
            if ($cuadrosClinicosItem->getCuadrosClinicos() === $this) {
                $cuadrosClinicosItem->setCuadrosClinicos(null);
            }
        }

        return $this;
    }
}
