<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EliminacionUrinariaRepository")
 */
class EliminacionUrinaria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Valoraciones", mappedBy="eliminacionesUrinarias")
     */
    private $valoraciones;

    public function __construct()
    {
        $this->valoraciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Valoraciones[]
     */
    public function getValoraciones(): Collection
    {
        return $this->valoraciones;
    }

    public function addValoracione(Valoraciones $valoracione): self
    {
        if (!$this->valoraciones->contains($valoracione)) {
            $this->valoraciones[] = $valoracione;
            $valoracione->addEliminacionesUrinaria($this);
        }

        return $this;
    }

    public function removeValoracione(Valoraciones $valoracione): self
    {
        if ($this->valoraciones->contains($valoracione)) {
            $this->valoraciones->removeElement($valoracione);
            $valoracione->removeEliminacionesUrinaria($this);
        }

        return $this;
    }
    public function __toString(){
      return $this->nombre;
    }
}
