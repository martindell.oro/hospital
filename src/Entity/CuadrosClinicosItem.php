<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CuadrosClinicosItemRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CuadrosClinicosItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    // /**
    //  * @ORM\ManyToMany(targetEntity="App\Entity\CuadrosClinicos", inversedBy="cuadrosClinicosItems")
    //  */
    // private $cuadroClinico;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $hora;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temperaturaAxila;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temperaturaRectal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $respiracion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pulso;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tA;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dolor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $glasgow;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $peso;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ingresosViaParenteral;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ingresosSng;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ingresosOral;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ingresosOtros;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ingresosTotal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosOrina;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosCatarsis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosVomito;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosDrenaje;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosSng;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosOtros;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $egresosTotal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CuadrosClinicos", inversedBy="cuadrosClinicosItems")
     */
    private $cuadrosClinicos;

    public function __construct()
    {
        $this->fecha = new \DateTime();
        $this->hora = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getHora(): ?\DateTimeInterface
    {
        return $this->hora;
    }

    public function setHora(?\DateTimeInterface $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getTemperaturaAxila(): ?string
    {
        return $this->temperaturaAxila;
    }

    public function setTemperaturaAxila(?string $temperaturaAxila): self
    {
        $this->temperaturaAxila = $temperaturaAxila;

        return $this;
    }

    public function getTemperaturaRectal(): ?string
    {
        return $this->temperaturaRectal;
    }

    public function setTemperaturaRectal(?string $temperaturaRectal): self
    {
        $this->temperaturaRectal = $temperaturaRectal;

        return $this;
    }

    public function getRespiracion(): ?string
    {
        return $this->respiracion;
    }

    public function setRespiracion(?string $respiracion): self
    {
        $this->respiracion = $respiracion;

        return $this;
    }

    public function getPulso(): ?string
    {
        return $this->pulso;
    }

    public function setPulso(?string $pulso): self
    {
        $this->pulso = $pulso;

        return $this;
    }

    public function getTA(): ?string
    {
        return $this->tA;
    }

    public function setTA(?string $tA): self
    {
        $this->tA = $tA;

        return $this;
    }

    public function getDolor(): ?int
    {
        return $this->dolor;
    }

    public function setDolor(?int $dolor): self
    {
        $this->dolor = $dolor;

        return $this;
    }

    public function getGlasgow(): ?string
    {
        return $this->glasgow;
    }

    public function setGlasgow(?string $glasgow): self
    {
        $this->glasgow = $glasgow;

        return $this;
    }

    public function getPeso(): ?int
    {
        return $this->peso;
    }

    public function setPeso(?int $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getIngresosViaParenteral(): ?string
    {
        return $this->ingresosViaParenteral;
    }

    public function setIngresosViaParenteral(?string $ingresosViaParenteral): self
    {
        $this->ingresosViaParenteral = $ingresosViaParenteral;

        return $this;
    }

    public function getIngresosSng(): ?string
    {
        return $this->ingresosSng;
    }

    public function setIngresosSng(?string $ingresosSng): self
    {
        $this->ingresosSng = $ingresosSng;

        return $this;
    }

    public function getIngresosOral(): ?string
    {
        return $this->ingresosOral;
    }

    public function setIngresosOral(?string $ingresosOral): self
    {
        $this->ingresosOral = $ingresosOral;

        return $this;
    }

    public function getIngresosOtros(): ?string
    {
        return $this->ingresosOtros;
    }

    public function setIngresosOtros(?string $ingresosOtros): self
    {
        $this->ingresosOtros = $ingresosOtros;

        return $this;
    }

    public function getIngresosTotal(): ?string
    {
        return $this->ingresosTotal;
    }

    public function setIngresosTotal(?string $ingresosTotal): self
    {
        $this->ingresosTotal = $ingresosTotal;

        return $this;
    }

    public function getEgresosOrina(): ?string
    {
        return $this->egresosOrina;
    }

    public function setEgresosOrina(?string $egresosOrina): self
    {
        $this->egresosOrina = $egresosOrina;

        return $this;
    }

    public function getEgresosCatarsis(): ?string
    {
        return $this->egresosCatarsis;
    }

    public function setEgresosCatarsis(?string $egresosCatarsis): self
    {
        $this->egresosCatarsis = $egresosCatarsis;

        return $this;
    }

    public function getEgresosVomito(): ?string
    {
        return $this->egresosVomito;
    }

    public function setEgresosVomito(?string $egresosVomito): self
    {
        $this->egresosVomito = $egresosVomito;

        return $this;
    }

    public function getEgresosDrenaje(): ?string
    {
        return $this->egresosDrenaje;
    }

    public function setEgresosDrenaje(?string $egresosDrenaje): self
    {
        $this->egresosDrenaje = $egresosDrenaje;

        return $this;
    }

    public function getEgresosSng(): ?string
    {
        return $this->egresosSng;
    }

    public function setEgresosSng(?string $egresosSng): self
    {
        $this->egresosSng = $egresosSng;

        return $this;
    }

    public function getEgresosOtros(): ?string
    {
        return $this->egresosOtros;
    }

    public function setEgresosOtros(?string $egresosOtros): self
    {
        $this->egresosOtros = $egresosOtros;

        return $this;
    }

    public function getEgresosTotal(): ?string
    {
        return $this->egresosTotal;
    }

    public function setEgresosTotal(?string $egresosTotal): self
    {
        $this->egresosTotal = $egresosTotal;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
      $this->createdAt = new \DateTime("now");
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
      $this->updatedAt = new \DateTime("now");
    }

    public function getCuadrosClinicos(): ?CuadrosClinicos
    {
        return $this->cuadrosClinicos;
    }

    public function setCuadrosClinicos(?CuadrosClinicos $cuadrosClinicos): self
    {
        $this->cuadrosClinicos = $cuadrosClinicos;

        return $this;
    }
}
