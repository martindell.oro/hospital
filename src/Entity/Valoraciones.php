<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ValoracionesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Valoraciones
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Pacientes", inversedBy="valoraciones")
     */
    private $paciente;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaIngreso;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $procedencia;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $forma_arribo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $forma_arribo_comentario;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tA;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temperatura;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $spo2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $glucemia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $glasgow;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Oxigenoterapia", inversedBy="valoraciones")
     */
    private $oxigenoTerapias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tuboNumero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicacionHabitual;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alergicoMedicamento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cirugias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fuenteInformacion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\IntegridadPielTergumentos", inversedBy="valoraciones")
     */
    private $pielTergumentos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pielTergumentosOtras;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Heridas", inversedBy="valoraciones")
     */
    private $heridas;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Nutriciones", inversedBy="valoraciones")
     */
    private $nutriciones;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EliminacionUrinaria", inversedBy="valoraciones")
     */
    private $eliminacionesUrinarias;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EliminacionInstestinal", inversedBy="valoraciones")
     */
    private $eliminacionesIntestinales;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AccesosPerifericos", inversedBy="valoraciones")
     */
    private $accesosPerifericos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EstadoConciencia", inversedBy="valoraciones")
     */
    private $estadoConciencia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estadoConcienciaOtro;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Comunicacion", inversedBy="valoraciones")
     */
    private $comunicacion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Vision", inversedBy="valoraciones")
     */
    private $vision;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Oidos", inversedBy="valoraciones")
     */
    private $oidos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Vacunas", inversedBy="valoraciones")
     */
    private $vacunas;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AntecedentesPersonales", inversedBy="valoraciones")
     */
    private $antecedentesPersonales;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $antecedentesPersonalesOtras;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $antecedentesPersonalesObservacion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EstudiosComplementarios", inversedBy="valoraciones")
     */
    private $estudiosComplementarios;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estudiosComplementariosOtros;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $horaLlegada;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $motivoConsulta;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medico;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaAplicacionAntigripal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EnfermeriaGuardia", mappedBy="valoraciones")
     */
    private $enfermeriaGuardias;

    public function __construct()
    {
        $this->fechaIngreso = new \DateTime();
        $this->horaLlegada =new \DateTime();
        $this->paciente = new ArrayCollection();
        $this->oxigenoTerapias = new ArrayCollection();
        $this->pielTergumentos = new ArrayCollection();
        $this->heridas = new ArrayCollection();
        $this->nutriciones = new ArrayCollection();
        $this->eliminacionesUrinarias = new ArrayCollection();
        $this->eliminacionesIntestinales = new ArrayCollection();
        $this->accesosPerifericos = new ArrayCollection();
        $this->estadoConciencia = new ArrayCollection();
        $this->comunicacion = new ArrayCollection();
        $this->vision = new ArrayCollection();
        $this->oidos = new ArrayCollection();
        $this->vacunas = new ArrayCollection();
        $this->antecedentesPersonales = new ArrayCollection();
        $this->estudiosComplementarios = new ArrayCollection();
        $this->enfermeriaGuardias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Pacientes[]
     */
    public function getPaciente(): Collection
    {
        return $this->paciente;
    }

    public function addPaciente(Pacientes $paciente): self
    {
        if (!$this->paciente->contains($paciente)) {
            $this->paciente[] = $paciente;
        }

        return $this;
    }

    public function removePaciente(Pacientes $paciente): self
    {
        if ($this->paciente->contains($paciente)) {
            $this->paciente->removeElement($paciente);
        }

        return $this;
    }

    public function getFechaIngreso(): ?\DateTimeInterface
    {
        return $this->fechaIngreso;
    }

    public function setFechaIngreso(\DateTimeInterface $fechaIngreso): self
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    public function getProcedencia(): ?string
    {
        return $this->procedencia;
    }

    public function setProcedencia(?string $procedencia): self
    {
        $this->procedencia = $procedencia;

        return $this;
    }

    public function getFormaArribo(): ?string
    {
        return $this->forma_arribo;
    }

    public function setFormaArribo(string $forma_arribo): self
    {
        $this->forma_arribo = $forma_arribo;

        return $this;
    }

    public function getFormaArriboComentario(): ?string
    {
        return $this->forma_arribo_comentario;
    }

    public function setFormaArriboComentario(?string $forma_arribo_comentario): self
    {
        $this->forma_arribo_comentario = $forma_arribo_comentario;

        return $this;
    }

    public function getTA(): ?string
    {
        return $this->tA;
    }

    public function setTA(?string $tA): self
    {
        $this->tA = $tA;

        return $this;
    }

    public function getFc(): ?string
    {
        return $this->fc;
    }

    public function setFc(?string $fc): self
    {
        $this->fc = $fc;

        return $this;
    }

    public function getFr(): ?string
    {
        return $this->fr;
    }

    public function setFr(?string $fr): self
    {
        $this->fr = $fr;

        return $this;
    }

    public function getTemperatura(): ?string
    {
        return $this->temperatura;
    }

    public function setTemperatura(?string $temperatura): self
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    public function getSpo2(): ?string
    {
        return $this->spo2;
    }

    public function setSpo2(?string $spo2): self
    {
        $this->spo2 = $spo2;

        return $this;
    }

    public function getGlucemia(): ?string
    {
        return $this->glucemia;
    }

    public function setGlucemia(?string $glucemia): self
    {
        $this->glucemia = $glucemia;

        return $this;
    }

    public function getGlasgow(): ?string
    {
        return $this->glasgow;
    }

    public function setGlasgow(?string $glasgow): self
    {
        $this->glasgow = $glasgow;

        return $this;
    }

    /**
     * @return Collection|Oxigenoterapia[]
     */
    public function getOxigenoTerapias(): Collection
    {
        return $this->oxigenoTerapias;
    }

    public function addOxigenoTerapia(Oxigenoterapia $oxigenoTerapia): self
    {
        if (!$this->oxigenoTerapias->contains($oxigenoTerapia)) {
            $this->oxigenoTerapias[] = $oxigenoTerapia;
        }

        return $this;
    }

    public function removeOxigenoTerapia(Oxigenoterapia $oxigenoTerapia): self
    {
        if ($this->oxigenoTerapias->contains($oxigenoTerapia)) {
            $this->oxigenoTerapias->removeElement($oxigenoTerapia);
        }

        return $this;
    }

    public function getTuboNumero(): ?string
    {
        return $this->tuboNumero;
    }

    public function setTuboNumero(?string $tuboNumero): self
    {
        $this->tuboNumero = $tuboNumero;

        return $this;
    }

    public function getMedicacionHabitual(): ?string
    {
        return $this->medicacionHabitual;
    }

    public function setMedicacionHabitual(?string $medicacionHabitual): self
    {
        $this->medicacionHabitual = $medicacionHabitual;

        return $this;
    }

    public function getAlergicoMedicamento(): ?string
    {
        return $this->alergicoMedicamento;
    }

    public function setAlergicoMedicamento(?string $alergicoMedicamento): self
    {
        $this->alergicoMedicamento = $alergicoMedicamento;

        return $this;
    }

    public function getCirugias(): ?string
    {
        return $this->cirugias;
    }

    public function setCirugias(?string $cirugias): self
    {
        $this->cirugias = $cirugias;

        return $this;
    }

    public function getFuenteInformacion(): ?string
    {
        return $this->fuenteInformacion;
    }

    public function setFuenteInformacion(?string $fuenteInformacion): self
    {
        $this->fuenteInformacion = $fuenteInformacion;

        return $this;
    }

    /**
     * @return Collection|IntegridadPielTergumentos[]
     */
    public function getPielTergumentos(): Collection
    {
        return $this->pielTergumentos;
    }

    public function addPielTergumento(IntegridadPielTergumentos $pielTergumento): self
    {
        if (!$this->pielTergumentos->contains($pielTergumento)) {
            $this->pielTergumentos[] = $pielTergumento;
        }

        return $this;
    }

    public function removePielTergumento(IntegridadPielTergumentos $pielTergumento): self
    {
        if ($this->pielTergumentos->contains($pielTergumento)) {
            $this->pielTergumentos->removeElement($pielTergumento);
        }

        return $this;
    }

    public function getPielTergumentosOtras(): ?string
    {
        return $this->pielTergumentosOtras;
    }

    public function setPielTergumentosOtras(?string $pielTergumentosOtras): self
    {
        $this->pielTergumentosOtras = $pielTergumentosOtras;

        return $this;
    }

    /**
     * @return Collection|Heridas[]
     */
    public function getHeridas(): Collection
    {
        return $this->heridas;
    }

    public function addHerida(Heridas $herida): self
    {
        if (!$this->heridas->contains($herida)) {
            $this->heridas[] = $herida;
        }

        return $this;
    }

    public function removeHerida(Heridas $herida): self
    {
        if ($this->heridas->contains($herida)) {
            $this->heridas->removeElement($herida);
        }

        return $this;
    }

    /**
     * @return Collection|Nutriciones[]
     */
    public function getNutriciones(): Collection
    {
        return $this->nutriciones;
    }

    public function addNutricione(Nutriciones $nutricione): self
    {
        if (!$this->nutriciones->contains($nutricione)) {
            $this->nutriciones[] = $nutricione;
        }

        return $this;
    }

    public function removeNutricione(Nutriciones $nutricione): self
    {
        if ($this->nutriciones->contains($nutricione)) {
            $this->nutriciones->removeElement($nutricione);
        }

        return $this;
    }

    /**
     * @return Collection|EliminacionUrinaria[]
     */
    public function getEliminacionesUrinarias(): Collection
    {
        return $this->eliminacionesUrinarias;
    }

    public function addEliminacionesUrinaria(EliminacionUrinaria $eliminacionesUrinaria): self
    {
        if (!$this->eliminacionesUrinarias->contains($eliminacionesUrinaria)) {
            $this->eliminacionesUrinarias[] = $eliminacionesUrinaria;
        }

        return $this;
    }

    public function removeEliminacionesUrinaria(EliminacionUrinaria $eliminacionesUrinaria): self
    {
        if ($this->eliminacionesUrinarias->contains($eliminacionesUrinaria)) {
            $this->eliminacionesUrinarias->removeElement($eliminacionesUrinaria);
        }

        return $this;
    }

    /**
     * @return Collection|EliminacionInstestinal[]
     */
    public function getEliminacionesIntestinales(): Collection
    {
        return $this->eliminacionesIntestinales;
    }

    public function addEliminacionesIntestinale(EliminacionInstestinal $eliminacionesIntestinale): self
    {
        if (!$this->eliminacionesIntestinales->contains($eliminacionesIntestinale)) {
            $this->eliminacionesIntestinales[] = $eliminacionesIntestinale;
        }

        return $this;
    }

    public function removeEliminacionesIntestinale(EliminacionInstestinal $eliminacionesIntestinale): self
    {
        if ($this->eliminacionesIntestinales->contains($eliminacionesIntestinale)) {
            $this->eliminacionesIntestinales->removeElement($eliminacionesIntestinale);
        }

        return $this;
    }

    /**
     * @return Collection|AccesosPerifericos[]
     */
    public function getAccesosPerifericos(): Collection
    {
        return $this->accesosPerifericos;
    }

    public function addAccesosPeriferico(AccesosPerifericos $accesosPeriferico): self
    {
        if (!$this->accesosPerifericos->contains($accesosPeriferico)) {
            $this->accesosPerifericos[] = $accesosPeriferico;
        }

        return $this;
    }

    public function removeAccesosPeriferico(AccesosPerifericos $accesosPeriferico): self
    {
        if ($this->accesosPerifericos->contains($accesosPeriferico)) {
            $this->accesosPerifericos->removeElement($accesosPeriferico);
        }

        return $this;
    }

    /**
     * @return Collection|EstadoConciencia[]
     */
    public function getEstadoConciencia(): Collection
    {
        return $this->estadoConciencia;
    }

    public function addEstadoConciencium(EstadoConciencia $estadoConciencium): self
    {
        if (!$this->estadoConciencia->contains($estadoConciencium)) {
            $this->estadoConciencia[] = $estadoConciencium;
        }

        return $this;
    }

    public function removeEstadoConciencium(EstadoConciencia $estadoConciencium): self
    {
        if ($this->estadoConciencia->contains($estadoConciencium)) {
            $this->estadoConciencia->removeElement($estadoConciencium);
        }

        return $this;
    }

    public function getEstadoConcienciaOtro(): ?string
    {
        return $this->estadoConcienciaOtro;
    }

    public function setEstadoConcienciaOtro(?string $estadoConcienciaOtro): self
    {
        $this->estadoConcienciaOtro = $estadoConcienciaOtro;

        return $this;
    }

    /**
     * @return Collection|Comunicacion[]
     */
    public function getComunicacion(): Collection
    {
        return $this->comunicacion;
    }

    public function addComunicacion(Comunicacion $comunicacion): self
    {
        if (!$this->comunicacion->contains($comunicacion)) {
            $this->comunicacion[] = $comunicacion;
        }

        return $this;
    }

    public function removeComunicacion(Comunicacion $comunicacion): self
    {
        if ($this->comunicacion->contains($comunicacion)) {
            $this->comunicacion->removeElement($comunicacion);
        }

        return $this;
    }

    /**
     * @return Collection|Vision[]
     */
    public function getVision(): Collection
    {
        return $this->vision;
    }

    public function addVision(Vision $vision): self
    {
        if (!$this->vision->contains($vision)) {
            $this->vision[] = $vision;
        }

        return $this;
    }

    public function removeVision(Vision $vision): self
    {
        if ($this->vision->contains($vision)) {
            $this->vision->removeElement($vision);
        }

        return $this;
    }

    /**
     * @return Collection|Oidos[]
     */
    public function getOidos(): Collection
    {
        return $this->oidos;
    }

    public function addOido(Oidos $oido): self
    {
        if (!$this->oidos->contains($oido)) {
            $this->oidos[] = $oido;
        }

        return $this;
    }

    public function removeOido(Oidos $oido): self
    {
        if ($this->oidos->contains($oido)) {
            $this->oidos->removeElement($oido);
        }

        return $this;
    }

    /**
     * @return Collection|Vacunas[]
     */
    public function getVacunas(): Collection
    {
        return $this->vacunas;
    }

    public function addVacuna(Vacunas $vacuna): self
    {
        if (!$this->vacunas->contains($vacuna)) {
            $this->vacunas[] = $vacuna;
        }

        return $this;
    }

    public function removeVacuna(Vacunas $vacuna): self
    {
        if ($this->vacunas->contains($vacuna)) {
            $this->vacunas->removeElement($vacuna);
        }

        return $this;
    }

    /**
     * @return Collection|AntecedentesPersonales[]
     */
    public function getAntecedentesPersonales(): Collection
    {
        return $this->antecedentesPersonales;
    }

    public function addAntecedentesPersonale(AntecedentesPersonales $antecedentesPersonale): self
    {
        if (!$this->antecedentesPersonales->contains($antecedentesPersonale)) {
            $this->antecedentesPersonales[] = $antecedentesPersonale;
        }

        return $this;
    }

    public function removeAntecedentesPersonale(AntecedentesPersonales $antecedentesPersonale): self
    {
        if ($this->antecedentesPersonales->contains($antecedentesPersonale)) {
            $this->antecedentesPersonales->removeElement($antecedentesPersonale);
        }

        return $this;
    }

    public function getAntecedentesPersonalesOtras(): ?string
    {
        return $this->antecedentesPersonalesOtras;
    }

    public function setAntecedentesPersonalesOtras(?string $antecedentesPersonalesOtras): self
    {
        $this->antecedentesPersonalesOtras = $antecedentesPersonalesOtras;

        return $this;
    }

    public function getAntecedentesPersonalesObservacion(): ?string
    {
        return $this->antecedentesPersonalesObservacion;
    }

    public function setAntecedentesPersonalesObservacion(?string $antecedentesPersonalesObservacion): self
    {
        $this->antecedentesPersonalesObservacion = $antecedentesPersonalesObservacion;

        return $this;
    }

    /**
     * @return Collection|EstudiosComplementarios[]
     */
    public function getEstudiosComplementarios(): Collection
    {
        return $this->estudiosComplementarios;
    }

    public function addEstudiosComplementario(EstudiosComplementarios $estudiosComplementario): self
    {
        if (!$this->estudiosComplementarios->contains($estudiosComplementario)) {
            $this->estudiosComplementarios[] = $estudiosComplementario;
        }

        return $this;
    }

    public function removeEstudiosComplementario(EstudiosComplementarios $estudiosComplementario): self
    {
        if ($this->estudiosComplementarios->contains($estudiosComplementario)) {
            $this->estudiosComplementarios->removeElement($estudiosComplementario);
        }

        return $this;
    }

    public function getEstudiosComplementariosOtros(): ?string
    {
        return $this->estudiosComplementariosOtros;
    }

    public function setEstudiosComplementariosOtros(?string $estudiosComplementariosOtros): self
    {
        $this->estudiosComplementariosOtros = $estudiosComplementariosOtros;

        return $this;
    }

    public function getHoraLlegada(): ?\DateTimeInterface
    {
        return $this->horaLlegada;
    }

    public function setHoraLlegada(?\DateTimeInterface $horaLlegada): self
    {
        $this->horaLlegada = $horaLlegada;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getMotivoConsulta(): ?string
    {
        return $this->motivoConsulta;
    }

    public function setMotivoConsulta(?string $motivoConsulta): self
    {
        $this->motivoConsulta = $motivoConsulta;

        return $this;
    }


      /**
       * Gets triggered only on insert

       * @ORM\PrePersist
       */
      public function onPrePersist()
      {
          $this->createdAt = new \DateTime("now");
          $this->updatedAt = new \DateTime("now");
      }

      /**
       * Gets triggered every time on update

       * @ORM\PreUpdate
       */
      public function onPreUpdate()
      {
          $this->updatedAt = new \DateTime("now");
      }

      public function getMedico(): ?string
      {
          return $this->medico;
      }

      public function setMedico(?string $medico): self
      {
          $this->medico = $medico;

          return $this;
      }

      public function getFechaAplicacionAntigripal(): ?\DateTimeInterface
      {
          return $this->fechaAplicacionAntigripal;
      }

      public function setFechaAplicacionAntigripal(?\DateTimeInterface $fechaAplicacionAntigripal): self
      {
          $this->fechaAplicacionAntigripal = $fechaAplicacionAntigripal;

          return $this;
      }

      /**
       * @return Collection|EnfermeriaGuardia[]
       */
      public function getEnfermeriaGuardias(): Collection
      {
          return $this->enfermeriaGuardias;
      }

      public function addEnfermeriaGuardia(EnfermeriaGuardia $enfermeriaGuardia): self
      {
          if (!$this->enfermeriaGuardias->contains($enfermeriaGuardia)) {
              $this->enfermeriaGuardias[] = $enfermeriaGuardia;
              $enfermeriaGuardia->setValoraciones($this);
          }

          return $this;
      }

      public function removeEnfermeriaGuardia(EnfermeriaGuardia $enfermeriaGuardia): self
      {
          if ($this->enfermeriaGuardias->contains($enfermeriaGuardia)) {
              $this->enfermeriaGuardias->removeElement($enfermeriaGuardia);
              // set the owning side to null (unless already changed)
              if ($enfermeriaGuardia->getValoraciones() === $this) {
                  $enfermeriaGuardia->setValoraciones(null);
              }
          }

          return $this;
      }
}
