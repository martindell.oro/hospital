<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KardecsItemsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class KardecsItems
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Kardecs", inversedBy="kardecsItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kardec;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dosis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $via;

    /**
     * @ORM\Column(type="datetime")
     */
    private $horario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistorialKardecs", mappedBy="kardecItem")
     */
    private $historialKardecs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $frecuencia;

    public function __construct()
    {
        $this->horario = new \DateTime();
        $this->historialKardecs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKardec(): ?Kardecs
    {
        return $this->kardec;
    }

    public function setKardec(?Kardecs $kardec): self
    {
        $this->kardec = $kardec;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDosis(): ?string
    {
        return $this->dosis;
    }

    public function setDosis(?string $dosis): self
    {
        $this->dosis = $dosis;

        return $this;
    }

    public function getVia(): ?string
    {
        return $this->via;
    }

    public function setVia(?string $via): self
    {
        $this->via = $via;

        return $this;
    }

    public function getHorario(): ?\DateTimeInterface
    {
        return $this->horario;
    }

    public function setHorario(\DateTimeInterface $horario): self
    {
        $this->horario = $horario;

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|HistorialKardecs[]
     */
    public function getFecha(): Collection
    {
        return $this->fecha;
    }

    public function addFecha(HistorialKardecs $fecha): self
    {
        if (!$this->fecha->contains($fecha)) {
            $this->fecha[] = $fecha;
            $fecha->setKardecItem($this);
        }

        return $this;
    }

    public function removeFecha(HistorialKardecs $fecha): self
    {
        if ($this->fecha->contains($fecha)) {
            $this->fecha->removeElement($fecha);
            // set the owning side to null (unless already changed)
            if ($fecha->getKardecItem() === $this) {
                $fecha->setKardecItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HistorialKardecs[]
     */
    public function getHistorialKardecs(): Collection
    {
        return $this->historialKardecs;
    }

    public function addHistorialKardec(HistorialKardecs $historialKardec): self
    {
        if (!$this->historialKardecs->contains($historialKardec)) {
            $this->historialKardecs[] = $historialKardec;
            $historialKardec->setKardecItem($this);
        }

        return $this;
    }

    public function removeHistorialKardec(HistorialKardecs $historialKardec): self
    {
        if ($this->historialKardecs->contains($historialKardec)) {
            $this->historialKardecs->removeElement($historialKardec);
            // set the owning side to null (unless already changed)
            if ($historialKardec->getKardecItem() === $this) {
                $historialKardec->setKardecItem(null);
            }
        }

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
      $this->createdAt = new \DateTime("now");
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
      $this->updatedAt = new \DateTime("now");
    }

    public function getFrecuencia(): ?int
    {
        return $this->frecuencia;
    }

    public function setFrecuencia(?int $frecuencia): self
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }
}
