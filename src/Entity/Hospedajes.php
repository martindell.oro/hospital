<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HospedajesRepository")
 */
class Hospedajes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $provincia;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $departamento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefono;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacidadMaxima;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservas", mappedBy="hospedaje")
     */
    private $reservas;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $institucionHospitalaria;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cantidadRespiradores;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="instituciones")
     */
    private $users;

    public function __construct()
    {
        $this->reservas = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getDepartamento(): ?string
    {
        return $this->departamento;
    }

    public function setDepartamento(string $departamento): self
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCapacidadMaxima(): ?int
    {
        return $this->capacidadMaxima;
    }

    public function setCapacidadMaxima(int $capacidadMaxima): self
    {
        $this->capacidadMaxima = $capacidadMaxima;

        return $this;
    }

    /**
     * @return Collection|Reservas[]
     */
    public function getReservas(): Collection
    {
        return $this->reservas;
    }

    public function addReserva(Reservas $reserva): self
    {
        if (!$this->reservas->contains($reserva)) {
            $this->reservas[] = $reserva;
            $reserva->setHospedaje($this);
        }

        return $this;
    }

    public function removeReserva(Reservas $reserva): self
    {
        if ($this->reservas->contains($reserva)) {
            $this->reservas->removeElement($reserva);
            // set the owning side to null (unless already changed)
            if ($reserva->getHospedaje() === $this) {
                $reserva->setHospedaje(null);
            }
        }

        return $this;
    }
    public function __toString(){
      return $this->nombre;
    }

    public function getInstitucionHospitalaria(): ?bool
    {
        return $this->institucionHospitalaria;
    }

    public function setInstitucionHospitalaria(?bool $institucionHospitalaria): self
    {
        $this->institucionHospitalaria = $institucionHospitalaria;

        return $this;
    }

    public function getCantidadRespiradores(): ?int
    {
        return $this->cantidadRespiradores;
    }

    public function setCantidadRespiradores(?int $cantidadRespiradores): self
    {
        $this->cantidadRespiradores = $cantidadRespiradores;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addInstitucione($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeInstitucione($this);
        }

        return $this;
    }
}
