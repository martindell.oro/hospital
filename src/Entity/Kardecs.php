<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KardecsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Kardecs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaIngreso;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $diagnostico;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pacientes", inversedBy="kardecs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $paciente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\KardecsItems", mappedBy="kardec")
     */
    private $kardecsItems;

    public function __construct()
    {
        $this->fechaIngreso = new \DateTime();
        $this->kardecsItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaIngreso(): ?\DateTimeInterface
    {
        return $this->fechaIngreso;
    }

    public function setFechaIngreso(?\DateTimeInterface $fechaIngreso): self
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    public function getDiagnostico(): ?string
    {
        return $this->diagnostico;
    }

    public function setDiagnostico(?string $diagnostico): self
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    public function getPaciente(): ?Pacientes
    {
        return $this->paciente;
    }

    public function setPaciente(?Pacientes $paciente): self
    {
        $this->paciente = $paciente;

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|KardecsItems[]
     */
    public function getKardecsItems(): Collection
    {
        return $this->kardecsItems;
    }

    public function addKardecsItem(KardecsItems $kardecsItem): self
    {
        if (!$this->kardecsItems->contains($kardecsItem)) {
            $this->kardecsItems[] = $kardecsItem;
            $kardecsItem->setKardec($this);
        }

        return $this;
    }

    public function removeKardecsItem(KardecsItems $kardecsItem): self
    {
        if ($this->kardecsItems->contains($kardecsItem)) {
            $this->kardecsItems->removeElement($kardecsItem);
            // set the owning side to null (unless already changed)
            if ($kardecsItem->getKardec() === $this) {
                $kardecsItem->setKardec(null);
            }
        }

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
      $this->createdAt = new \DateTime("now");
      $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
      $this->updatedAt = new \DateTime("now");
    }
}
