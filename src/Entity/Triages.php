<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TriagesRepository")
 */
class Triages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Pacientes", inversedBy="triage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $paciente;

    /**
     * @ORM\Column(type="boolean")
     */
    private $viajo_paises_infectados;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_ingreso_pais;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $contacto_directo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $viajo_zona_transmision;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observacion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sintomas")
     */
    private $sintomas;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_inicio_sintomas;

    /**
     * @ORM\Column(type="boolean")
     */
    private $patologia_base;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $servicioEsencial;

    public function __construct()
    {
        $this->sintomas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaciente(): ?Pacientes
    {
        return $this->paciente;
    }

    public function setPaciente(Pacientes $paciente): self
    {
        $this->paciente = $paciente;

        return $this;
    }

    public function getViajoPaisesInfectados(): ?bool
    {
        return $this->viajo_paises_infectados;
    }

    public function setViajoPaisesInfectados(bool $viajo_paises_infectados): self
    {
        $this->viajo_paises_infectados = $viajo_paises_infectados;

        return $this;
    }

    public function getFechaIngresoPais(): ?\DateTimeInterface
    {
        return $this->fecha_ingreso_pais;
    }

    public function setFechaIngresoPais(?\DateTimeInterface $fecha_ingreso_pais): self
    {
        $this->fecha_ingreso_pais = $fecha_ingreso_pais;

        return $this;
    }

    public function getContactoDirecto(): ?bool
    {
        return $this->contacto_directo;
    }

    public function setContactoDirecto(?bool $contacto_directo): self
    {
        $this->contacto_directo = $contacto_directo;

        return $this;
    }

    public function getViajoZonaTransmision(): ?bool
    {
        return $this->viajo_zona_transmision;
    }

    public function setViajoZonaTransmision(bool $viajo_zona_transmision): self
    {
        $this->viajo_zona_transmision = $viajo_zona_transmision;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * @return Collection|Sintomas[]
     */
    public function getSintomas(): Collection
    {
        return $this->sintomas;
    }

    public function addSintoma(Sintomas $sintoma): self
    {
        if (!$this->sintomas->contains($sintoma)) {
            $this->sintomas[] = $sintoma;
        }

        return $this;
    }

    public function removeSintoma(Sintomas $sintoma): self
    {
        if ($this->sintomas->contains($sintoma)) {
            $this->sintomas->removeElement($sintoma);
        }

        return $this;
    }

    public function getFechaInicioSintomas(): ?\DateTimeInterface
    {
        return $this->fecha_inicio_sintomas;
    }

    public function setFechaInicioSintomas(?\DateTimeInterface $fecha_inicio_sintomas): self
    {
        $this->fecha_inicio_sintomas = $fecha_inicio_sintomas;

        return $this;
    }

    public function getPatologiaBase(): ?bool
    {
        return $this->patologia_base;
    }

    public function setPatologiaBase(bool $patologia_base): self
    {
        $this->patologia_base = $patologia_base;

        return $this;
    }

    public function getServicioEsencial(): ?bool
    {
        return $this->servicioEsencial;
    }

    public function setServicioEsencial(?bool $servicioEsencial): self
    {
        $this->servicioEsencial = $servicioEsencial;

        return $this;
    }
}
