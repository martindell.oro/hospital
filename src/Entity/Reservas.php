<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservasRepository")
  * @ORM\HasLifecycleCallbacks
 */
class Reservas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pacientes", inversedBy="reservas")
     */
    private $paciente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hospedajes", inversedBy="reservas")
     */
    private $hospedaje;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaDesde;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaHasta;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checkout;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $respirador;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $habitacion;

    public function __construct()
    {
        $this->fechaDesde = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaciente(): ?Pacientes
    {
        return $this->paciente;
    }

    public function setPaciente(?Pacientes $paciente): self
    {
        $this->paciente = $paciente;

        return $this;
    }

    public function getHospedaje(): ?Hospedajes
    {
        return $this->hospedaje;
    }

    public function setHospedaje(?Hospedajes $hospedaje): self
    {
        $this->hospedaje = $hospedaje;

        return $this;
    }

    public function getFechaDesde(): ?\DateTimeInterface
    {
        return $this->fechaDesde;
    }

    public function setFechaDesde(\DateTimeInterface $fechaDesde): self
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    public function getFechaHasta(): ?\DateTimeInterface
    {
        return $this->fechaHasta;
    }

    public function setFechaHasta(?\DateTimeInterface $fechaHasta): self
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    public function getCheckout(): ?bool
    {
        return $this->checkout;
    }

    public function setCheckout(?bool $checkout): self
    {
        $this->checkout = $checkout;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getRespirador(): ?bool
    {
        return $this->respirador;
    }

    public function setRespirador(?bool $respirador): self
    {
        $this->respirador = $respirador;

        return $this;
    }

    public function getHabitacion(): ?string
    {
        return $this->habitacion;
    }

    public function setHabitacion(string $habitacion): self
    {
        $this->habitacion = $habitacion;

        return $this;
    }

}
