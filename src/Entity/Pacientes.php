<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PacientesRepository")
 * @UniqueEntity(
 *    fields={"dni"},
 *    message="Este DNI ya ha sido cargado"
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Pacientes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellido;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $provincia;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $domicilio;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_nacimiento;

    /**
     * @ORM\Column(type="boolean")
     */
    private $requiere_internacion;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Triages", mappedBy="paciente", cascade={"persist", "remove"})
     */
    private $triage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservas", mappedBy="paciente")
     */
    private $reservas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sexo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $obraSocial;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Valoraciones", mappedBy="paciente")
     */
    private $valoraciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CuadrosClinicos", mappedBy="paciente")
     */
    private $cuadrosClinicos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $historialClinico;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anamnesis", mappedBy="paciente")
     */
    private $anamnesis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PrescripcionesOrdenes", mappedBy="paciente")
     */
    private $prescripcionesOrdenes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Kardecs", mappedBy="paciente")
     */
    private $kardecs;

    public function __construct()
    {
        $this->reservas = new ArrayCollection();
        $this->valoraciones = new ArrayCollection();
        $this->cuadrosClinicos = new ArrayCollection();
        $this->anamnesis = new ArrayCollection();
        $this->prescripcionesOrdenes = new ArrayCollection();
        $this->kardecs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getDni(): ?int
    {
        return $this->dni;
    }

    public function setDni(int $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getDomicilio(): ?string
    {
        return $this->domicilio;
    }

    public function setDomicilio(string $domicilio): self
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fecha_nacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fecha_nacimiento): self
    {
        $this->fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function getRequiereInternacion(): ?bool
    {
        return $this->requiere_internacion;
    }

    public function setRequiereInternacion(bool $requiere_internacion): self
    {
        $this->requiere_internacion = $requiere_internacion;

        return $this;
    }

    public function getTriage(): ?Triages
    {
        return $this->triage;
    }

    public function __toString()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    /**
     * @return Collection|Reservas[]
     */
    public function getReservas(): Collection
    {
        return $this->reservas;
    }

    public function addReserva(Reservas $reserva): self
    {
        if (!$this->reservas->contains($reserva)) {
            $this->reservas[] = $reserva;
            $reserva->setPaciente($this);
        }

        return $this;
    }

    public function removeReserva(Reservas $reserva): self
    {
        if ($this->reservas->contains($reserva)) {
            $this->reservas->removeElement($reserva);
            // set the owning side to null (unless already changed)
            if ($reserva->getPaciente() === $this) {
                $reserva->setPaciente(null);
            }
        }

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getObraSocial(): ?string
    {
        return $this->obraSocial;
    }

    public function setObraSocial(?string $obraSocial): self
    {
        $this->obraSocial = $obraSocial;

        return $this;
    }

    /**
     * @return Collection|Valoraciones[]
     */
    public function getValoraciones(): Collection
    {
        return $this->valoraciones;
    }

    public function addValoracione(Valoraciones $valoracione): self
    {
        if (!$this->valoraciones->contains($valoracione)) {
            $this->valoraciones[] = $valoracione;
            $valoracione->addPaciente($this);
        }

        return $this;
    }

    public function removeValoracione(Valoraciones $valoracione): self
    {
        if ($this->valoraciones->contains($valoracione)) {
            $this->valoraciones->removeElement($valoracione);
            $valoracione->removePaciente($this);
        }

        return $this;
    }

    /**
     * @return Collection|CuadrosClinicos[]
     */
    public function getCuadrosClinicos(): Collection
    {
        return $this->cuadrosClinicos;
    }

    public function addCuadrosClinico(CuadrosClinicos $cuadrosClinico): self
    {
        if (!$this->cuadrosClinicos->contains($cuadrosClinico)) {
            $this->cuadrosClinicos[] = $cuadrosClinico;
            $cuadrosClinico->setPaciente($this);
        }

        return $this;
    }

    public function removeCuadrosClinico(CuadrosClinicos $cuadrosClinico): self
    {
        if ($this->cuadrosClinicos->contains($cuadrosClinico)) {
            $this->cuadrosClinicos->removeElement($cuadrosClinico);
            // set the owning side to null (unless already changed)
            if ($cuadrosClinico->getPaciente() === $this) {
                $cuadrosClinico->setPaciente(null);
            }
        }

        return $this;
    }

    public function getHistorialClinico(): ?string
    {
        return $this->historialClinico;
    }

    public function setHistorialClinico(?string $historialClinico): self
    {
        $this->historialClinico = $historialClinico;

        return $this;
    }

    /**
     * @return Collection|Anamnesis[]
     */
    public function getAnamnesis(): Collection
    {
        return $this->anamnesis;
    }

    public function addAnamnesi(Anamnesis $anamnesi): self
    {
        if (!$this->anamnesis->contains($anamnesi)) {
            $this->anamnesis[] = $anamnesi;
            $anamnesi->setPaciente($this);
        }

        return $this;
    }

    public function removeAnamnesi(Anamnesis $anamnesi): self
    {
        if ($this->anamnesis->contains($anamnesi)) {
            $this->anamnesis->removeElement($anamnesi);
            // set the owning side to null (unless already changed)
            if ($anamnesi->getPaciente() === $this) {
                $anamnesi->setPaciente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PrescripcionesOrdenes[]
     */
    public function getPrescripcionesOrdenes(): Collection
    {
        return $this->prescripcionesOrdenes;
    }

    public function addPrescripcionesOrdene(PrescripcionesOrdenes $prescripcionesOrdene): self
    {
        if (!$this->prescripcionesOrdenes->contains($prescripcionesOrdene)) {
            $this->prescripcionesOrdenes[] = $prescripcionesOrdene;
            $prescripcionesOrdene->setPaciente($this);
        }

        return $this;
    }

    public function removePrescripcionesOrdene(PrescripcionesOrdenes $prescripcionesOrdene): self
    {
        if ($this->prescripcionesOrdenes->contains($prescripcionesOrdene)) {
            $this->prescripcionesOrdenes->removeElement($prescripcionesOrdene);
            // set the owning side to null (unless already changed)
            if ($prescripcionesOrdene->getPaciente() === $this) {
                $prescripcionesOrdene->setPaciente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Kardecs[]
     */
    public function getKardecs(): Collection
    {
        return $this->kardecs;
    }

    public function addKardec(Kardecs $kardec): self
    {
        if (!$this->kardecs->contains($kardec)) {
            $this->kardecs[] = $kardec;
            $kardec->setPaciente($this);
        }

        return $this;
    }

    public function removeKardec(Kardecs $kardec): self
    {
        if ($this->kardecs->contains($kardec)) {
            $this->kardecs->removeElement($kardec);
            // set the owning side to null (unless already changed)
            if ($kardec->getPaciente() === $this) {
                $kardec->setPaciente(null);
            }
        }

        return $this;
    }
}
