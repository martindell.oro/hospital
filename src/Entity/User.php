<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Hospedajes", inversedBy="users")
     */
    private $instituciones;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellido;

    public function __construct()
    {
        parent::__construct();
        $this->instituciones = new ArrayCollection();
    }

    /**
     * @return Collection|Hospedajes[]
     */
    public function getInstituciones(): Collection
    {
        return $this->instituciones;
    }

    public function addInstitucione(Hospedajes $institucione): self
    {
        if (!$this->instituciones->contains($institucione)) {
            $this->instituciones[] = $institucione;
        }

        return $this;
    }

    public function removeInstitucione(Hospedajes $institucione): self
    {
        if ($this->instituciones->contains($institucione)) {
            $this->instituciones->removeElement($institucione);
        }

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }
}
