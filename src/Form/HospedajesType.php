<?php

namespace App\Form;

use App\Entity\Hospedajes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class HospedajesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('provincia',TextType::class, array('data'=>'Entre Ríos'))
            ->add('departamento',TextType::class, array('data'=>'Federación'))
            ->add('localidad',TextType::class, array('data'=>'Chajarí'))
            ->add('direccion')
            ->add('telefono')
            ->add('capacidadMaxima')
            ->add('institucionHospitalaria', CheckboxType::class, array('required'=>false, 'data'=>false,'label'=>'¿Pertenece a una Institución Hospitalaria? (Es Hospital/Clinica Privada/Centro de Salud/ etc)'))
            ->add('cantidadRespiradores')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Hospedajes::class,
        ]);
    }
}
