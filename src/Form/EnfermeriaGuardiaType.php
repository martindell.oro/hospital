<?php

namespace App\Form;

use App\Entity\EnfermeriaGuardia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EnfermeriaGuardiaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class, array('label'=>'Fecha','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('hora',TimeType::class, array('label'=>'Hora'))
            ->add('observaciones',TextareaType::class, array('label'=>'Tratamiento y Observaciones'))
            ->add('dosis')
            ->add('via')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EnfermeriaGuardia::class,
        ]);
    }
}
