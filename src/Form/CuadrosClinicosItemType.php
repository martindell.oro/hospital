<?php

namespace App\Form;

use App\Entity\CuadrosClinicosItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class CuadrosClinicosItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->add('fecha',DateType::class, array('label'=>'Fecha','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
          ->add('hora',TimeType::class, array('label'=>'Hora'))
          ->add('temperaturaAxila')
          ->add('temperaturaRectal')
          ->add('respiracion')
          ->add('pulso')
          ->add('tA')
          ->add('dolor')
          ->add('glasgow')
          ->add('peso')
          ->add('ingresosViaParenteral')
          ->add('ingresosSng')
          ->add('ingresosOral')
          ->add('ingresosOtros')
          ->add('ingresosTotal')
          ->add('egresosOrina')
          ->add('egresosCatarsis')
          ->add('egresosVomito')
          ->add('egresosDrenaje')
          ->add('egresosSng')
          ->add('egresosOtros')
          ->add('egresosTotal')
          ->add('observaciones')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CuadrosClinicosItem::class,
        ]);
    }
}
