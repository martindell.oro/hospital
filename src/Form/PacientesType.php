<?php

namespace App\Form;

use App\Entity\Pacientes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class PacientesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('dni')
            ->add('obraSocial')
            ->add('historialClinico',TextType::class, array('required'=>false, 'label'=>'Historial Clínico Nro.'))
            ->add('pais',TextType::class, array('data'=>'Argentina'))
            ->add('provincia',TextType::class, array('data'=>'Entre Rios'))
            ->add('localidad',TextType::class, array('data'=>'Chajarí'))
            ->add('domicilio')
            ->add('telefono')
            ->add('sexo', ChoiceType::class, [
                'choices'  => [
                    'Masculino' => 'masculino',
                    'Femenino' => 'femenino',
                ],
            ])
            ->add('fecha_nacimiento',DateType::class, array('widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('requiere_internacion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pacientes::class,
        ]);
    }
}
