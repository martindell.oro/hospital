<?php

namespace App\Form;

use App\Entity\SeguimientoObservaciones;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SeguimientoObservacionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class, array('label'=>'Fecha', 'required'=> false,'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('observacion')
            ->add('sintomas')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeguimientoObservaciones::class,
        ]);
    }
}
