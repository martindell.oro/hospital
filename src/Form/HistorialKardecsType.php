<?php

namespace App\Form;

use App\Entity\HistorialKardecs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;



class HistorialKardecsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class, array('label'=>'Fecha Ingreso','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('hora',TimeType::class, array('label'=>'Hora'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HistorialKardecs::class,
        ]);
    }
}
