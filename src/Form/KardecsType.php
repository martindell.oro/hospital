<?php

namespace App\Form;

use App\Entity\Kardecs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class KardecsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaIngreso',DateType::class, array('label'=>'Fecha Ingreso','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('diagnostico')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Kardecs::class,
        ]);
    }
}
