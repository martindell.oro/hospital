<?php

namespace App\Form;

use App\Entity\Valoraciones;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ValoracionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaIngreso',DateType::class, array('label'=>'Fecha Ingreso','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('horaLlegada',TimeType::class, array('label'=>false))
            ->add('motivoConsulta',TextareaType::class, array('required'=>false,'label'=>'Motivo de Consulta'))
            ->add('medico',TextareaType::class, array('required'=>false,'label'=>'Médico'))
            ->add('procedencia', ChoiceType::class, [
                'choices'  => [
                    'Consultorio Externo' => 'Consultorio Externo',
                    'Ambulancia' => 'Ambulancia',
                    'Enfermero' => 'Enfermero',
                    'Particular' => 'Particular',
                    'Derivado' => 'Derivado',
                    'Médico' => 'Médico',
                    'Guardia' => 'Guardia',
                    'Policía' => 'Policía',
                    'Oficio' => 'Oficio',
                    'Aviso' => 'Aviso'
                ],
            ])
            ->add('forma_arribo', ChoiceType::class, [
                'choices'  => [
                    'Camilla' => 'Camilla',
                    'Silla' => 'Silla',
                    'Collar cervical'=> 'Collar cervical',
                    'Tabla'=> 'Tabla',
                    'Acompañado'=> 'Acompañado',
                ],
                'label'=>'Forma de Arribo'
            ])
            ->add('forma_arribo_comentario',TextareaType::class, array('required'=>false, 'label'=>'Observación forma de arribo'))
            ->add('tA',TextType::class, array('required'=>false,'label'=>'T/A'))
            ->add('fc',TextType::class, array('required'=>false,'label'=>'FC'))
            ->add('fr',TextType::class, array('required'=>false,'label'=>'FR'))
            ->add('temperatura',TextType::class, array('required'=>false,'label'=>'T°'))
            ->add('spo2',TextType::class, array('required'=>false,'label'=>'SPO2'))
            ->add('glucemia',TextType::class, array('required'=>false,'label'=>'Glucemia'))
            ->add('glasgow',TextType::class, array('required'=>false,'label'=>'Glasgow'))
            ->add('oxigenoTerapias')
            ->add('tuboNumero',TextType::class, array('required'=>false,'label'=>'Tubo número'))
            ->add('medicacionHabitual',TextType::class, array('required'=>false,'label'=>'Medicación Habitual'))
            ->add('alergicoMedicamento',TextType::class, array('required'=>false,'label'=>'Alérgico a algún medicamento'))
            ->add('cirugias',TextType::class, array('required'=>false,'label'=>'Cirugías'))
            ->add('fuenteInformacion',TextType::class, array('required'=>false,'label'=>'Fuente de Información'))
            ->add('pielTergumentos')
            ->add('pielTergumentosOtras')
            ->add('heridas')
            ->add('nutriciones')
            ->add('eliminacionesUrinarias')
            ->add('eliminacionesIntestinales')
            ->add('accesosPerifericos')
            ->add('estadoConciencia')
            ->add('estadoConcienciaOtro')
            ->add('comunicacion')
            ->add('vision')
            ->add('oidos')
            ->add('vacunas')
            ->add('fechaAplicacionAntigripal',DateType::class, array('label'=>'Fecha de Aplicación Antigripal','required'=> false, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('antecedentesPersonales')
            ->add('antecedentesPersonalesOtras')
            ->add('antecedentesPersonalesObservacion')
            ->add('estudiosComplementarios')
            ->add('estudiosComplementariosOtros')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Valoraciones::class,
        ]);
    }
}
