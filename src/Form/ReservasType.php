<?php

namespace App\Form;

use App\Entity\Reservas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ReservasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaDesde',DateType::class, array('label'=>'Fecha desde','required'=> true, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('hospedaje', CheckboxType::class, array('required'=>false,'label'=>'Institucion/Hospedaje','attr'=>array('class'=>'mdb-select md-form')))
            ->add('respirador', CheckboxType::class, array('required'=>false,'label'=>'¿Necesita Respirador?','attr'=>array('id'=>'respirador')))
            ->add('habitacion',TextType::class, array('label'=>'Número de Habitación o Cama'))
            ->add('hospedaje')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservas::class,
        ]);
    }
}
