<?php

namespace App\Form;

use App\Entity\KardecsItems;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class KardecsItemsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion')
            ->add('dosis')
            ->add('via')
            ->add('horario',TimeType::class, array('label'=>'Hora'))
            ->add('frecuencia',IntegerType::class, array('label'=>'Frecuencia: (cuántas veces al día)', 'attr' => array('min' => 1, 'max' => 24)))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KardecsItems::class,
        ]);
    }
}
