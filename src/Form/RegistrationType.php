<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\DataTransformer\StringToArrayTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Entity\Hospedajes;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $transformer = new StringToArrayTransformer();
      $builder
        ->add('nombre')
        ->add('apellido')
        ->add('instituciones',EntityType::class, array(
          'class' => Hospedajes::class,
          'multiple' => true,
          'query_builder' => function (EntityRepository $er) {
              return $er->createQueryBuilder('u')
                  ->where('u.institucionHospitalaria = TRUE')
                  ->orderBy('u.nombre', 'ASC');
          },
        ))
        ->add('roles',ChoiceType::class, array(
          'choices' => array(
                         'Administrador' => 'ROLE_ADMIN',
                         'Enfermero' => 'ROLE_ENFERMERO',
                         'Médico' => 'ROLE_MEDICO',
                     ),
         'expanded' => false,
         'multiple'=>true,
        ))
      ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'user_register';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
