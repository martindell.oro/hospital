<?php

namespace App\Form;

use App\Entity\Seguimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SeguimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('fechaNacimiento',DateType::class, array('label'=>'Fecha de Nacimiento', 'required'=> false,'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('provincia',TextType::class, array('data'=>'Entre Rios'))
            ->add('localidad',TextType::class, array('data'=>'Chajarí'))
            ->add('domicilio')
            ->add('dni')
            ->add('telefono')
            ->add('fechaArribo',DateType::class, array('label'=>'Fecha de Arribo', 'required'=> false,'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')))
            ->add('procedencia')
            ->add('finalizoSeguimiento')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Seguimiento::class,
        ]);
    }
}
