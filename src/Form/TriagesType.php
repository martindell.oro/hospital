<?php

namespace App\Form;

use App\Entity\Triages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class TriagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('viajo_paises_infectados',CheckboxType::class, array('label'=>'¿Viajó en los últimos 14 días a Brasil, Chile, EEUU, Europa, Japón, Irán, Coreal del Sur, China?', 'required'=> false))
            ->add('fecha_ingreso_pais',DateType::class, array('label'=>'Fecha de Ingreso al país', 'required'=> false,'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('contacto_directo',CheckboxType::class, array('label'=>'¿Estuvo en contacto con alguna persona que viajó a alguno de estos lugares?', 'required'=> false))
            ->add('viajo_zona_transmision',CheckboxType::class, array('label'=>'¿Estuvo en alguna zona de transmisión?', 'required'=> false))
            ->add('fecha_inicio_sintomas',DateType::class, array('label'=>'Fecha de Inicio de Síntomas','required'=> false, 'widget'=>'single_text','format' => 'dd/MM/yyyy', 'html5'=> false, 'attr'=>array('class' => 'datepicker')) )
            ->add('sintomas')
            ->add('patologia_base',CheckboxType::class, array('label'=>'¿Tiene alguna patología de base (ASMA, EPOC, HTA, DBT, Oncológico)? ', 'required'=> false))
            ->add('servicioEsencial',CheckboxType::class, array('label'=>'¿Desempeña Servicio Esencial? ', 'required'=> false))
            ->add('observacion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Triages::class,
        ]);
    }
}
