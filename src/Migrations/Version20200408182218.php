<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200408182218 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE user_hospedajes (user_id INT NOT NULL, hospedajes_id INT NOT NULL, PRIMARY KEY(user_id, hospedajes_id))');
        $this->addSql('CREATE INDEX IDX_E867CCA3A76ED395 ON user_hospedajes (user_id)');
        $this->addSql('CREATE INDEX IDX_E867CCA3E768E708 ON user_hospedajes (hospedajes_id)');
        $this->addSql('ALTER TABLE user_hospedajes ADD CONSTRAINT FK_E867CCA3A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_hospedajes ADD CONSTRAINT FK_E867CCA3E768E708 FOREIGN KEY (hospedajes_id) REFERENCES hospedajes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE user_hospedajes');
    }
}
