<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200530100456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE seguimiento_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE seguimiento_observaciones_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE seguimiento (id INT NOT NULL, nombre VARCHAR(255) DEFAULT NULL, apellido VARCHAR(255) DEFAULT NULL, fecha_nacimiento DATE DEFAULT NULL, provincia VARCHAR(255) DEFAULT NULL, localidad VARCHAR(255) DEFAULT NULL, domicilio VARCHAR(255) DEFAULT NULL, dni VARCHAR(255) DEFAULT NULL, telefono VARCHAR(255) DEFAULT NULL, fecha_arribo DATE DEFAULT NULL, procedencia VARCHAR(1000) DEFAULT NULL, finalizo_seguimiento BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE seguimiento_observaciones (id INT NOT NULL, seguimiento_id INT DEFAULT NULL, fecha DATE DEFAULT NULL, observacion TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8B4C36BD5CBBFEB ON seguimiento_observaciones (seguimiento_id)');
        $this->addSql('CREATE TABLE seguimiento_observaciones_sintomas (seguimiento_observaciones_id INT NOT NULL, sintomas_id INT NOT NULL, PRIMARY KEY(seguimiento_observaciones_id, sintomas_id))');
        $this->addSql('CREATE INDEX IDX_CD695E5CE1812E1 ON seguimiento_observaciones_sintomas (seguimiento_observaciones_id)');
        $this->addSql('CREATE INDEX IDX_CD695E5C104202DD ON seguimiento_observaciones_sintomas (sintomas_id)');
        $this->addSql('ALTER TABLE seguimiento_observaciones ADD CONSTRAINT FK_8B4C36BD5CBBFEB FOREIGN KEY (seguimiento_id) REFERENCES seguimiento (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE seguimiento_observaciones_sintomas ADD CONSTRAINT FK_CD695E5CE1812E1 FOREIGN KEY (seguimiento_observaciones_id) REFERENCES seguimiento_observaciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE seguimiento_observaciones_sintomas ADD CONSTRAINT FK_CD695E5C104202DD FOREIGN KEY (sintomas_id) REFERENCES sintomas (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE seguimiento_observaciones DROP CONSTRAINT FK_8B4C36BD5CBBFEB');
        $this->addSql('ALTER TABLE seguimiento_observaciones_sintomas DROP CONSTRAINT FK_CD695E5CE1812E1');
        $this->addSql('DROP SEQUENCE seguimiento_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE seguimiento_observaciones_id_seq CASCADE');
        $this->addSql('DROP TABLE seguimiento');
        $this->addSql('DROP TABLE seguimiento_observaciones');
        $this->addSql('DROP TABLE seguimiento_observaciones_sintomas');
    }
}
