<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510132640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE kardecs_items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE historial_kardecs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sintomas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE kardecs_items (id INT NOT NULL, kardec_id INT NOT NULL, usuario_id INT NOT NULL, descripcion VARCHAR(255) DEFAULT NULL, dosis VARCHAR(255) DEFAULT NULL, via VARCHAR(255) DEFAULT NULL, horario TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_640F41572DAD9B4E ON kardecs_items (kardec_id)');
        $this->addSql('CREATE INDEX IDX_640F4157DB38439E ON kardecs_items (usuario_id)');
        $this->addSql('CREATE TABLE historial_kardecs (id INT NOT NULL, kardec_item_id INT DEFAULT NULL, usuario_id INT NOT NULL, fecha DATE DEFAULT NULL, hora TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2A52A4E4B8622A16 ON historial_kardecs (kardec_item_id)');
        $this->addSql('CREATE INDEX IDX_2A52A4E4DB38439E ON historial_kardecs (usuario_id)');
        $this->addSql('ALTER TABLE kardecs_items ADD CONSTRAINT FK_640F41572DAD9B4E FOREIGN KEY (kardec_id) REFERENCES kardecs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kardecs_items ADD CONSTRAINT FK_640F4157DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historial_kardecs ADD CONSTRAINT FK_2A52A4E4B8622A16 FOREIGN KEY (kardec_item_id) REFERENCES kardecs_items (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historial_kardecs ADD CONSTRAINT FK_2A52A4E4DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE historial_kardecs DROP CONSTRAINT FK_2A52A4E4B8622A16');
        $this->addSql('DROP SEQUENCE kardecs_items_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE historial_kardecs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sintomas_id_seq CASCADE');
        $this->addSql('DROP TABLE kardecs_items');
        $this->addSql('DROP TABLE historial_kardecs');
    }
}
