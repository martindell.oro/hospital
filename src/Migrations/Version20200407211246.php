<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407211246 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE reservas ADD respirador BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE reservas ADD habitacion VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE hospedajes ADD institucion_hospitalaria BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE hospedajes ADD cantidad_respiradores INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE hospedajes DROP institucion_hospitalaria');
        $this->addSql('ALTER TABLE hospedajes DROP cantidad_respiradores');
        $this->addSql('ALTER TABLE reservas DROP respirador');
        $this->addSql('ALTER TABLE reservas DROP habitacion');
    }
}
