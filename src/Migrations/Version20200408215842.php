<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200408215842 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE nutriciones_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE oidos_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE eliminacion_urinaria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE accesos_perifericos_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE heridas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE valoraciones_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE estudios_complementarios_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vacunas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE antecedentes_personales_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE comunicacion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vision_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE estado_conciencia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE eliminacion_instestinal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE nutriciones (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE oidos (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE eliminacion_urinaria (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE accesos_perifericos (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE heridas (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE valoraciones (id INT NOT NULL, fecha_ingreso TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, procedencia VARCHAR(255) DEFAULT NULL, forma_arribo VARCHAR(255) NOT NULL, forma_arribo_comentario TEXT DEFAULT NULL, t_a VARCHAR(255) DEFAULT NULL, fc VARCHAR(255) DEFAULT NULL, fr VARCHAR(255) DEFAULT NULL, temperatura VARCHAR(255) DEFAULT NULL, spo2 VARCHAR(255) DEFAULT NULL, glucemia VARCHAR(255) DEFAULT NULL, glasgow VARCHAR(255) DEFAULT NULL, tubo_numero VARCHAR(255) DEFAULT NULL, medicacion_habitual VARCHAR(255) DEFAULT NULL, alergico_medicamento VARCHAR(255) DEFAULT NULL, cirugias VARCHAR(255) DEFAULT NULL, fuente_informacion VARCHAR(255) DEFAULT NULL, piel_tergumentos_otras VARCHAR(255) DEFAULT NULL, estado_conciencia_otro VARCHAR(255) DEFAULT NULL, antecedentes_personales_otras VARCHAR(255) DEFAULT NULL, antecedentes_personales_observacion VARCHAR(255) DEFAULT NULL, estudios_complementarios_otros VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE valoraciones_pacientes (valoraciones_id INT NOT NULL, pacientes_id INT NOT NULL, PRIMARY KEY(valoraciones_id, pacientes_id))');
        $this->addSql('CREATE INDEX IDX_C5764108E051C8FC ON valoraciones_pacientes (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_C576410866BCBB32 ON valoraciones_pacientes (pacientes_id)');
        $this->addSql('CREATE TABLE valoraciones_oxigenoterapia (valoraciones_id INT NOT NULL, oxigenoterapia_id INT NOT NULL, PRIMARY KEY(valoraciones_id, oxigenoterapia_id))');
        $this->addSql('CREATE INDEX IDX_67FAC29AE051C8FC ON valoraciones_oxigenoterapia (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_67FAC29AB34ADEFC ON valoraciones_oxigenoterapia (oxigenoterapia_id)');
        $this->addSql('CREATE TABLE valoraciones_integridad_piel_tergumentos (valoraciones_id INT NOT NULL, integridad_piel_tergumentos_id INT NOT NULL, PRIMARY KEY(valoraciones_id, integridad_piel_tergumentos_id))');
        $this->addSql('CREATE INDEX IDX_5F97091FE051C8FC ON valoraciones_integridad_piel_tergumentos (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_5F97091F464125B ON valoraciones_integridad_piel_tergumentos (integridad_piel_tergumentos_id)');
        $this->addSql('CREATE TABLE valoraciones_heridas (valoraciones_id INT NOT NULL, heridas_id INT NOT NULL, PRIMARY KEY(valoraciones_id, heridas_id))');
        $this->addSql('CREATE INDEX IDX_993CF168E051C8FC ON valoraciones_heridas (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_993CF16810A87723 ON valoraciones_heridas (heridas_id)');
        $this->addSql('CREATE TABLE valoraciones_nutriciones (valoraciones_id INT NOT NULL, nutriciones_id INT NOT NULL, PRIMARY KEY(valoraciones_id, nutriciones_id))');
        $this->addSql('CREATE INDEX IDX_FAE5A5A7E051C8FC ON valoraciones_nutriciones (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_FAE5A5A7C9C4AD40 ON valoraciones_nutriciones (nutriciones_id)');
        $this->addSql('CREATE TABLE valoraciones_eliminacion_urinaria (valoraciones_id INT NOT NULL, eliminacion_urinaria_id INT NOT NULL, PRIMARY KEY(valoraciones_id, eliminacion_urinaria_id))');
        $this->addSql('CREATE INDEX IDX_E2E2215DE051C8FC ON valoraciones_eliminacion_urinaria (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_E2E2215D5F20349F ON valoraciones_eliminacion_urinaria (eliminacion_urinaria_id)');
        $this->addSql('CREATE TABLE valoraciones_eliminacion_instestinal (valoraciones_id INT NOT NULL, eliminacion_instestinal_id INT NOT NULL, PRIMARY KEY(valoraciones_id, eliminacion_instestinal_id))');
        $this->addSql('CREATE INDEX IDX_C293D17E051C8FC ON valoraciones_eliminacion_instestinal (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_C293D1728E5ABAD ON valoraciones_eliminacion_instestinal (eliminacion_instestinal_id)');
        $this->addSql('CREATE TABLE valoraciones_accesos_perifericos (valoraciones_id INT NOT NULL, accesos_perifericos_id INT NOT NULL, PRIMARY KEY(valoraciones_id, accesos_perifericos_id))');
        $this->addSql('CREATE INDEX IDX_78445163E051C8FC ON valoraciones_accesos_perifericos (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_78445163DFFFE5EB ON valoraciones_accesos_perifericos (accesos_perifericos_id)');
        $this->addSql('CREATE TABLE valoraciones_estado_conciencia (valoraciones_id INT NOT NULL, estado_conciencia_id INT NOT NULL, PRIMARY KEY(valoraciones_id, estado_conciencia_id))');
        $this->addSql('CREATE INDEX IDX_7CE7B965E051C8FC ON valoraciones_estado_conciencia (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_7CE7B965E0E76B6 ON valoraciones_estado_conciencia (estado_conciencia_id)');
        $this->addSql('CREATE TABLE valoraciones_comunicacion (valoraciones_id INT NOT NULL, comunicacion_id INT NOT NULL, PRIMARY KEY(valoraciones_id, comunicacion_id))');
        $this->addSql('CREATE INDEX IDX_F0759DC2E051C8FC ON valoraciones_comunicacion (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_F0759DC2312EA0C2 ON valoraciones_comunicacion (comunicacion_id)');
        $this->addSql('CREATE TABLE valoraciones_vision (valoraciones_id INT NOT NULL, vision_id INT NOT NULL, PRIMARY KEY(valoraciones_id, vision_id))');
        $this->addSql('CREATE INDEX IDX_D118F83E051C8FC ON valoraciones_vision (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_D118F831F4FDCB ON valoraciones_vision (vision_id)');
        $this->addSql('CREATE TABLE valoraciones_oidos (valoraciones_id INT NOT NULL, oidos_id INT NOT NULL, PRIMARY KEY(valoraciones_id, oidos_id))');
        $this->addSql('CREATE INDEX IDX_454D760DE051C8FC ON valoraciones_oidos (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_454D760D1A4ADAC8 ON valoraciones_oidos (oidos_id)');
        $this->addSql('CREATE TABLE valoraciones_vacunas (valoraciones_id INT NOT NULL, vacunas_id INT NOT NULL, PRIMARY KEY(valoraciones_id, vacunas_id))');
        $this->addSql('CREATE INDEX IDX_7F94872EE051C8FC ON valoraciones_vacunas (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_7F94872EC5D7731D ON valoraciones_vacunas (vacunas_id)');
        $this->addSql('CREATE TABLE valoraciones_antecedentes_personales (valoraciones_id INT NOT NULL, antecedentes_personales_id INT NOT NULL, PRIMARY KEY(valoraciones_id, antecedentes_personales_id))');
        $this->addSql('CREATE INDEX IDX_A65C0105E051C8FC ON valoraciones_antecedentes_personales (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_A65C010564DB2055 ON valoraciones_antecedentes_personales (antecedentes_personales_id)');
        $this->addSql('CREATE TABLE valoraciones_estudios_complementarios (valoraciones_id INT NOT NULL, estudios_complementarios_id INT NOT NULL, PRIMARY KEY(valoraciones_id, estudios_complementarios_id))');
        $this->addSql('CREATE INDEX IDX_DF4BB377E051C8FC ON valoraciones_estudios_complementarios (valoraciones_id)');
        $this->addSql('CREATE INDEX IDX_DF4BB3773B645FEB ON valoraciones_estudios_complementarios (estudios_complementarios_id)');
        $this->addSql('CREATE TABLE estudios_complementarios (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE vacunas (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE antecedentes_personales (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE comunicacion (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE vision (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE estado_conciencia (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE eliminacion_instestinal (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE valoraciones_pacientes ADD CONSTRAINT FK_C5764108E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_pacientes ADD CONSTRAINT FK_C576410866BCBB32 FOREIGN KEY (pacientes_id) REFERENCES pacientes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_oxigenoterapia ADD CONSTRAINT FK_67FAC29AE051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_oxigenoterapia ADD CONSTRAINT FK_67FAC29AB34ADEFC FOREIGN KEY (oxigenoterapia_id) REFERENCES oxigenoterapia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_integridad_piel_tergumentos ADD CONSTRAINT FK_5F97091FE051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_integridad_piel_tergumentos ADD CONSTRAINT FK_5F97091F464125B FOREIGN KEY (integridad_piel_tergumentos_id) REFERENCES integridad_piel_tergumentos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_heridas ADD CONSTRAINT FK_993CF168E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_heridas ADD CONSTRAINT FK_993CF16810A87723 FOREIGN KEY (heridas_id) REFERENCES heridas (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_nutriciones ADD CONSTRAINT FK_FAE5A5A7E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_nutriciones ADD CONSTRAINT FK_FAE5A5A7C9C4AD40 FOREIGN KEY (nutriciones_id) REFERENCES nutriciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_urinaria ADD CONSTRAINT FK_E2E2215DE051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_urinaria ADD CONSTRAINT FK_E2E2215D5F20349F FOREIGN KEY (eliminacion_urinaria_id) REFERENCES eliminacion_urinaria (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_instestinal ADD CONSTRAINT FK_C293D17E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_instestinal ADD CONSTRAINT FK_C293D1728E5ABAD FOREIGN KEY (eliminacion_instestinal_id) REFERENCES eliminacion_instestinal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_accesos_perifericos ADD CONSTRAINT FK_78445163E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_accesos_perifericos ADD CONSTRAINT FK_78445163DFFFE5EB FOREIGN KEY (accesos_perifericos_id) REFERENCES accesos_perifericos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_estado_conciencia ADD CONSTRAINT FK_7CE7B965E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_estado_conciencia ADD CONSTRAINT FK_7CE7B965E0E76B6 FOREIGN KEY (estado_conciencia_id) REFERENCES estado_conciencia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_comunicacion ADD CONSTRAINT FK_F0759DC2E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_comunicacion ADD CONSTRAINT FK_F0759DC2312EA0C2 FOREIGN KEY (comunicacion_id) REFERENCES comunicacion (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_vision ADD CONSTRAINT FK_D118F83E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_vision ADD CONSTRAINT FK_D118F831F4FDCB FOREIGN KEY (vision_id) REFERENCES vision (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_oidos ADD CONSTRAINT FK_454D760DE051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_oidos ADD CONSTRAINT FK_454D760D1A4ADAC8 FOREIGN KEY (oidos_id) REFERENCES oidos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_vacunas ADD CONSTRAINT FK_7F94872EE051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_vacunas ADD CONSTRAINT FK_7F94872EC5D7731D FOREIGN KEY (vacunas_id) REFERENCES vacunas (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_antecedentes_personales ADD CONSTRAINT FK_A65C0105E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_antecedentes_personales ADD CONSTRAINT FK_A65C010564DB2055 FOREIGN KEY (antecedentes_personales_id) REFERENCES antecedentes_personales (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_estudios_complementarios ADD CONSTRAINT FK_DF4BB377E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE valoraciones_estudios_complementarios ADD CONSTRAINT FK_DF4BB3773B645FEB FOREIGN KEY (estudios_complementarios_id) REFERENCES estudios_complementarios (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE valoraciones_nutriciones DROP CONSTRAINT FK_FAE5A5A7C9C4AD40');
        $this->addSql('ALTER TABLE valoraciones_oidos DROP CONSTRAINT FK_454D760D1A4ADAC8');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_urinaria DROP CONSTRAINT FK_E2E2215D5F20349F');
        $this->addSql('ALTER TABLE valoraciones_accesos_perifericos DROP CONSTRAINT FK_78445163DFFFE5EB');
        $this->addSql('ALTER TABLE valoraciones_heridas DROP CONSTRAINT FK_993CF16810A87723');
        $this->addSql('ALTER TABLE valoraciones_pacientes DROP CONSTRAINT FK_C5764108E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_oxigenoterapia DROP CONSTRAINT FK_67FAC29AE051C8FC');
        $this->addSql('ALTER TABLE valoraciones_integridad_piel_tergumentos DROP CONSTRAINT FK_5F97091FE051C8FC');
        $this->addSql('ALTER TABLE valoraciones_heridas DROP CONSTRAINT FK_993CF168E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_nutriciones DROP CONSTRAINT FK_FAE5A5A7E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_urinaria DROP CONSTRAINT FK_E2E2215DE051C8FC');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_instestinal DROP CONSTRAINT FK_C293D17E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_accesos_perifericos DROP CONSTRAINT FK_78445163E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_estado_conciencia DROP CONSTRAINT FK_7CE7B965E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_comunicacion DROP CONSTRAINT FK_F0759DC2E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_vision DROP CONSTRAINT FK_D118F83E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_oidos DROP CONSTRAINT FK_454D760DE051C8FC');
        $this->addSql('ALTER TABLE valoraciones_vacunas DROP CONSTRAINT FK_7F94872EE051C8FC');
        $this->addSql('ALTER TABLE valoraciones_antecedentes_personales DROP CONSTRAINT FK_A65C0105E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_estudios_complementarios DROP CONSTRAINT FK_DF4BB377E051C8FC');
        $this->addSql('ALTER TABLE valoraciones_estudios_complementarios DROP CONSTRAINT FK_DF4BB3773B645FEB');
        $this->addSql('ALTER TABLE valoraciones_vacunas DROP CONSTRAINT FK_7F94872EC5D7731D');
        $this->addSql('ALTER TABLE valoraciones_antecedentes_personales DROP CONSTRAINT FK_A65C010564DB2055');
        $this->addSql('ALTER TABLE valoraciones_comunicacion DROP CONSTRAINT FK_F0759DC2312EA0C2');
        $this->addSql('ALTER TABLE valoraciones_vision DROP CONSTRAINT FK_D118F831F4FDCB');
        $this->addSql('ALTER TABLE valoraciones_estado_conciencia DROP CONSTRAINT FK_7CE7B965E0E76B6');
        $this->addSql('ALTER TABLE valoraciones_eliminacion_instestinal DROP CONSTRAINT FK_C293D1728E5ABAD');
        $this->addSql('DROP SEQUENCE nutriciones_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE oidos_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE eliminacion_urinaria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE accesos_perifericos_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE heridas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE valoraciones_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE estudios_complementarios_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vacunas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE antecedentes_personales_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE comunicacion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vision_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE estado_conciencia_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE eliminacion_instestinal_id_seq CASCADE');
        $this->addSql('DROP TABLE nutriciones');
        $this->addSql('DROP TABLE oidos');
        $this->addSql('DROP TABLE eliminacion_urinaria');
        $this->addSql('DROP TABLE accesos_perifericos');
        $this->addSql('DROP TABLE heridas');
        $this->addSql('DROP TABLE valoraciones');
        $this->addSql('DROP TABLE valoraciones_pacientes');
        $this->addSql('DROP TABLE valoraciones_oxigenoterapia');
        $this->addSql('DROP TABLE valoraciones_integridad_piel_tergumentos');
        $this->addSql('DROP TABLE valoraciones_heridas');
        $this->addSql('DROP TABLE valoraciones_nutriciones');
        $this->addSql('DROP TABLE valoraciones_eliminacion_urinaria');
        $this->addSql('DROP TABLE valoraciones_eliminacion_instestinal');
        $this->addSql('DROP TABLE valoraciones_accesos_perifericos');
        $this->addSql('DROP TABLE valoraciones_estado_conciencia');
        $this->addSql('DROP TABLE valoraciones_comunicacion');
        $this->addSql('DROP TABLE valoraciones_vision');
        $this->addSql('DROP TABLE valoraciones_oidos');
        $this->addSql('DROP TABLE valoraciones_vacunas');
        $this->addSql('DROP TABLE valoraciones_antecedentes_personales');
        $this->addSql('DROP TABLE valoraciones_estudios_complementarios');
        $this->addSql('DROP TABLE estudios_complementarios');
        $this->addSql('DROP TABLE vacunas');
        $this->addSql('DROP TABLE antecedentes_personales');
        $this->addSql('DROP TABLE comunicacion');
        $this->addSql('DROP TABLE vision');
        $this->addSql('DROP TABLE estado_conciencia');
        $this->addSql('DROP TABLE eliminacion_instestinal');
    }
}
