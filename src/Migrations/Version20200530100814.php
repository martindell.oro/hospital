<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200530100814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE seguimiento ADD usuario_id INT NOT NULL');
        $this->addSql('ALTER TABLE seguimiento ADD CONSTRAINT FK_1B2181DDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1B2181DDB38439E ON seguimiento (usuario_id)');
        $this->addSql('ALTER TABLE seguimiento_observaciones ADD usuario_id INT NOT NULL');
        $this->addSql('ALTER TABLE seguimiento_observaciones ADD CONSTRAINT FK_8B4C36BDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8B4C36BDB38439E ON seguimiento_observaciones (usuario_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE seguimiento DROP CONSTRAINT FK_1B2181DDB38439E');
        $this->addSql('DROP INDEX IDX_1B2181DDB38439E');
        $this->addSql('ALTER TABLE seguimiento DROP usuario_id');
        $this->addSql('ALTER TABLE seguimiento_observaciones DROP CONSTRAINT FK_8B4C36BDB38439E');
        $this->addSql('DROP INDEX IDX_8B4C36BDB38439E');
        $this->addSql('ALTER TABLE seguimiento_observaciones DROP usuario_id');
    }
}
