<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200410141815 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE cuadros_clinicos_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE cuadros_clinicos (id INT NOT NULL, usuario_id INT DEFAULT NULL, fecha DATE DEFAULT NULL, hora TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, temperatura_axila VARCHAR(255) DEFAULT NULL, temperatura_rectal VARCHAR(255) DEFAULT NULL, respiracion VARCHAR(255) DEFAULT NULL, pulso VARCHAR(255) DEFAULT NULL, t_a VARCHAR(255) DEFAULT NULL, dolor INT DEFAULT NULL, glasgow VARCHAR(255) DEFAULT NULL, peso INT DEFAULT NULL, ingresos_via_parenteral VARCHAR(255) DEFAULT NULL, ingresos_sng VARCHAR(255) DEFAULT NULL, ingresos_oral VARCHAR(255) DEFAULT NULL, ingresos_otros VARCHAR(255) DEFAULT NULL, ingresos_total VARCHAR(255) DEFAULT NULL, egresos_orina VARCHAR(255) DEFAULT NULL, egresos_catarsis VARCHAR(255) DEFAULT NULL, egresos_vomito VARCHAR(255) NOT NULL, egresos_drenaje VARCHAR(255) DEFAULT NULL, egresos_sng VARCHAR(255) DEFAULT NULL, egresos_otros VARCHAR(255) DEFAULT NULL, egresos_total VARCHAR(255) DEFAULT NULL, observaciones TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_91E48950DB38439E ON cuadros_clinicos (usuario_id)');
        $this->addSql('CREATE TABLE cuadros_clinicos_pacientes (cuadros_clinicos_id INT NOT NULL, pacientes_id INT NOT NULL, PRIMARY KEY(cuadros_clinicos_id, pacientes_id))');
        $this->addSql('CREATE INDEX IDX_E740BF0347DBEE72 ON cuadros_clinicos_pacientes (cuadros_clinicos_id)');
        $this->addSql('CREATE INDEX IDX_E740BF0366BCBB32 ON cuadros_clinicos_pacientes (pacientes_id)');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD CONSTRAINT FK_91E48950DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_pacientes ADD CONSTRAINT FK_E740BF0347DBEE72 FOREIGN KEY (cuadros_clinicos_id) REFERENCES cuadros_clinicos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_pacientes ADD CONSTRAINT FK_E740BF0366BCBB32 FOREIGN KEY (pacientes_id) REFERENCES pacientes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE cuadros_clinicos_pacientes DROP CONSTRAINT FK_E740BF0347DBEE72');
        $this->addSql('DROP SEQUENCE cuadros_clinicos_id_seq CASCADE');
        $this->addSql('DROP TABLE cuadros_clinicos');
        $this->addSql('DROP TABLE cuadros_clinicos_pacientes');
    }
}
