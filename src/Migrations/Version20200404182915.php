<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200404182915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE sintomas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE pacientes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE triages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE sintomas (id INT NOT NULL, sintoma VARCHAR(255) NOT NULL, descripcion VARCHAR(1000) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE pacientes (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, dni INT NOT NULL, pais VARCHAR(255) NOT NULL, provincia VARCHAR(255) NOT NULL, localidad VARCHAR(255) NOT NULL, domicilio VARCHAR(1000) NOT NULL, fecha_nacimiento DATE NOT NULL, requiere_internacion BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE triages (id INT NOT NULL, paciente_id INT NOT NULL, viajo_paises_infectados BOOLEAN NOT NULL, fecha_ingreso_pais DATE DEFAULT NULL, contacto_directo BOOLEAN DEFAULT NULL, viajo_zona_transmision BOOLEAN NOT NULL, observacion TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_480760177310DAD4 ON triages (paciente_id)');
        $this->addSql('CREATE TABLE triages_sintomas (triages_id INT NOT NULL, sintomas_id INT NOT NULL, PRIMARY KEY(triages_id, sintomas_id))');
        $this->addSql('CREATE INDEX IDX_E1F9D1A384B18E89 ON triages_sintomas (triages_id)');
        $this->addSql('CREATE INDEX IDX_E1F9D1A3104202DD ON triages_sintomas (sintomas_id)');
        $this->addSql('ALTER TABLE triages ADD CONSTRAINT FK_480760177310DAD4 FOREIGN KEY (paciente_id) REFERENCES pacientes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE triages_sintomas ADD CONSTRAINT FK_E1F9D1A384B18E89 FOREIGN KEY (triages_id) REFERENCES triages (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE triages_sintomas ADD CONSTRAINT FK_E1F9D1A3104202DD FOREIGN KEY (sintomas_id) REFERENCES sintomas (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE triages_sintomas DROP CONSTRAINT FK_E1F9D1A3104202DD');
        $this->addSql('ALTER TABLE triages DROP CONSTRAINT FK_480760177310DAD4');
        $this->addSql('ALTER TABLE triages_sintomas DROP CONSTRAINT FK_E1F9D1A384B18E89');
        $this->addSql('DROP SEQUENCE sintomas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pacientes_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE triages_id_seq CASCADE');
        $this->addSql('DROP TABLE sintomas');
        $this->addSql('DROP TABLE pacientes');
        $this->addSql('DROP TABLE triages');
        $this->addSql('DROP TABLE triages_sintomas');
    }
}
