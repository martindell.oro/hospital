<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200411193019 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE cuadros_clinicos_item_cuadros_clinicos');
        $this->addSql('ALTER TABLE cuadros_clinicos_item ADD cuadros_clinicos_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos_item ADD CONSTRAINT FK_2CCDA05847DBEE72 FOREIGN KEY (cuadros_clinicos_id) REFERENCES cuadros_clinicos (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2CCDA05847DBEE72 ON cuadros_clinicos_item (cuadros_clinicos_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_item_id INT NOT NULL, cuadros_clinicos_id INT NOT NULL, PRIMARY KEY(cuadros_clinicos_item_id, cuadros_clinicos_id))');
        $this->addSql('CREATE INDEX idx_4003c94547dbee72 ON cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_id)');
        $this->addSql('CREATE INDEX idx_4003c9454f3cb8f0 ON cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_item_id)');
        $this->addSql('ALTER TABLE cuadros_clinicos_item_cuadros_clinicos ADD CONSTRAINT fk_4003c9454f3cb8f0 FOREIGN KEY (cuadros_clinicos_item_id) REFERENCES cuadros_clinicos_item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_item_cuadros_clinicos ADD CONSTRAINT fk_4003c94547dbee72 FOREIGN KEY (cuadros_clinicos_id) REFERENCES cuadros_clinicos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_item DROP CONSTRAINT FK_2CCDA05847DBEE72');
        $this->addSql('DROP INDEX IDX_2CCDA05847DBEE72');
        $this->addSql('ALTER TABLE cuadros_clinicos_item DROP cuadros_clinicos_id');
    }
}
