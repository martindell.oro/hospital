<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200413162537 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE anamnesis_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE anamnesis_items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE anamnesis (id INT NOT NULL, paciente_id INT NOT NULL, usuario_id INT NOT NULL, peso INT DEFAULT NULL, estatura DOUBLE PRECISION DEFAULT NULL, presion_sanguinea DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F0A580697310DAD4 ON anamnesis (paciente_id)');
        $this->addSql('CREATE INDEX IDX_F0A58069DB38439E ON anamnesis (usuario_id)');
        $this->addSql('CREATE TABLE anamnesis_items (id INT NOT NULL, usuario_id INT NOT NULL, descripcion TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3A4A7136DB38439E ON anamnesis_items (usuario_id)');
        $this->addSql('ALTER TABLE anamnesis ADD CONSTRAINT FK_F0A580697310DAD4 FOREIGN KEY (paciente_id) REFERENCES pacientes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE anamnesis ADD CONSTRAINT FK_F0A58069DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE anamnesis_items ADD CONSTRAINT FK_3A4A7136DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE anamnesis_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE anamnesis_items_id_seq CASCADE');
        $this->addSql('DROP TABLE anamnesis');
        $this->addSql('DROP TABLE anamnesis_items');
    }
}
