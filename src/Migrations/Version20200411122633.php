<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200411122633 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE cuadros_clinicos_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE cuadros_clinicos_item (id INT NOT NULL, usuario_id INT DEFAULT NULL, fecha DATE DEFAULT NULL, hora TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, temperatura_axila VARCHAR(255) DEFAULT NULL, temperatura_rectal VARCHAR(255) DEFAULT NULL, respiracion VARCHAR(255) DEFAULT NULL, pulso VARCHAR(255) DEFAULT NULL, t_a VARCHAR(255) DEFAULT NULL, dolor INT DEFAULT NULL, glasgow VARCHAR(255) DEFAULT NULL, peso INT DEFAULT NULL, ingresos_via_parenteral VARCHAR(255) DEFAULT NULL, ingresos_sng VARCHAR(255) DEFAULT NULL, ingresos_oral VARCHAR(255) DEFAULT NULL, ingresos_otros VARCHAR(255) DEFAULT NULL, ingresos_total VARCHAR(255) DEFAULT NULL, egresos_orina VARCHAR(255) DEFAULT NULL, egresos_catarsis VARCHAR(255) DEFAULT NULL, egresos_vomito VARCHAR(255) DEFAULT NULL, egresos_drenaje VARCHAR(255) DEFAULT NULL, egresos_sng VARCHAR(255) DEFAULT NULL, egresos_otros VARCHAR(255) DEFAULT NULL, egresos_total VARCHAR(255) DEFAULT NULL, observaciones TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2CCDA058DB38439E ON cuadros_clinicos_item (usuario_id)');
        $this->addSql('CREATE TABLE cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_item_id INT NOT NULL, cuadros_clinicos_id INT NOT NULL, PRIMARY KEY(cuadros_clinicos_item_id, cuadros_clinicos_id))');
        $this->addSql('CREATE INDEX IDX_4003C9454F3CB8F0 ON cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_item_id)');
        $this->addSql('CREATE INDEX IDX_4003C94547DBEE72 ON cuadros_clinicos_item_cuadros_clinicos (cuadros_clinicos_id)');
        $this->addSql('ALTER TABLE cuadros_clinicos_item ADD CONSTRAINT FK_2CCDA058DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_item_cuadros_clinicos ADD CONSTRAINT FK_4003C9454F3CB8F0 FOREIGN KEY (cuadros_clinicos_item_id) REFERENCES cuadros_clinicos_item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_item_cuadros_clinicos ADD CONSTRAINT FK_4003C94547DBEE72 FOREIGN KEY (cuadros_clinicos_id) REFERENCES cuadros_clinicos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP CONSTRAINT fk_91e48950db38439e');
        $this->addSql('DROP INDEX idx_91e48950db38439e');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD servicio VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD cama VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD sala VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP usuario_id');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP fecha');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP hora');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP temperatura_axila');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP temperatura_rectal');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP respiracion');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP pulso');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP t_a');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP dolor');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP glasgow');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP peso');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP ingresos_via_parenteral');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP ingresos_sng');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP ingresos_oral');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP ingresos_otros');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP ingresos_total');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_orina');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_catarsis');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_vomito');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_drenaje');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_sng');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_otros');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP egresos_total');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP observaciones');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE cuadros_clinicos_item_cuadros_clinicos DROP CONSTRAINT FK_4003C9454F3CB8F0');
        $this->addSql('DROP SEQUENCE cuadros_clinicos_item_id_seq CASCADE');
        $this->addSql('DROP TABLE cuadros_clinicos_item');
        $this->addSql('DROP TABLE cuadros_clinicos_item_cuadros_clinicos');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD usuario_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD fecha DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD hora TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD temperatura_axila VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD temperatura_rectal VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD respiracion VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD pulso VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD t_a VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD dolor INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD glasgow VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD peso INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD ingresos_via_parenteral VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD ingresos_sng VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD ingresos_oral VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD ingresos_otros VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD ingresos_total VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_orina VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_catarsis VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_vomito VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_drenaje VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_sng VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_otros VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD egresos_total VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD observaciones TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP servicio');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP cama');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP sala');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD CONSTRAINT fk_91e48950db38439e FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_91e48950db38439e ON cuadros_clinicos (usuario_id)');
    }
}
