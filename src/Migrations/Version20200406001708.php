<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200406001708 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE reservas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hospedajes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE reservas (id INT NOT NULL, paciente_id INT DEFAULT NULL, hospedaje_id INT DEFAULT NULL, fecha_desde DATE NOT NULL, fecha_hasta DATE DEFAULT NULL, checkout BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AA1DAB017310DAD4 ON reservas (paciente_id)');
        $this->addSql('CREATE INDEX IDX_AA1DAB01195A2D00 ON reservas (hospedaje_id)');
        $this->addSql('CREATE TABLE hospedajes (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, provincia VARCHAR(255) NOT NULL, departamento VARCHAR(255) NOT NULL, localidad VARCHAR(255) NOT NULL, direccion VARCHAR(255) NOT NULL, telefono VARCHAR(255) NOT NULL, capacidad_maxima INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE reservas ADD CONSTRAINT FK_AA1DAB017310DAD4 FOREIGN KEY (paciente_id) REFERENCES pacientes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reservas ADD CONSTRAINT FK_AA1DAB01195A2D00 FOREIGN KEY (hospedaje_id) REFERENCES hospedajes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pacientes ADD telefono VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE pacientes ADD sexo VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_971B78517F8F253B ON pacientes (dni)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE reservas DROP CONSTRAINT FK_AA1DAB01195A2D00');
        $this->addSql('DROP SEQUENCE reservas_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hospedajes_id_seq CASCADE');
        $this->addSql('DROP TABLE reservas');
        $this->addSql('DROP TABLE hospedajes');
        $this->addSql('DROP INDEX UNIQ_971B78517F8F253B');
        $this->addSql('ALTER TABLE pacientes DROP telefono');
        $this->addSql('ALTER TABLE pacientes DROP sexo');
    }
}
