<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200413181739 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE prescripciones_ordenes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE prescripciones_ordenes (id INT NOT NULL, paciente_id INT NOT NULL, usuario_id INT DEFAULT NULL, prescripciones DATE NOT NULL, hora TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, descripcion TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_947ADE8A7310DAD4 ON prescripciones_ordenes (paciente_id)');
        $this->addSql('CREATE INDEX IDX_947ADE8ADB38439E ON prescripciones_ordenes (usuario_id)');
        $this->addSql('ALTER TABLE prescripciones_ordenes ADD CONSTRAINT FK_947ADE8A7310DAD4 FOREIGN KEY (paciente_id) REFERENCES pacientes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prescripciones_ordenes ADD CONSTRAINT FK_947ADE8ADB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE prescripciones_ordenes_id_seq CASCADE');
        $this->addSql('DROP TABLE prescripciones_ordenes');
    }
}
