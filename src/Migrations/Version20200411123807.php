<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200411123807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE cuadros_clinicos_pacientes');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD paciente_id INT NOT NULL');
        $this->addSql('ALTER TABLE cuadros_clinicos ADD CONSTRAINT FK_91E489507310DAD4 FOREIGN KEY (paciente_id) REFERENCES pacientes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_91E489507310DAD4 ON cuadros_clinicos (paciente_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE cuadros_clinicos_pacientes (cuadros_clinicos_id INT NOT NULL, pacientes_id INT NOT NULL, PRIMARY KEY(cuadros_clinicos_id, pacientes_id))');
        $this->addSql('CREATE INDEX idx_e740bf0347dbee72 ON cuadros_clinicos_pacientes (cuadros_clinicos_id)');
        $this->addSql('CREATE INDEX idx_e740bf0366bcbb32 ON cuadros_clinicos_pacientes (pacientes_id)');
        $this->addSql('ALTER TABLE cuadros_clinicos_pacientes ADD CONSTRAINT fk_e740bf0347dbee72 FOREIGN KEY (cuadros_clinicos_id) REFERENCES cuadros_clinicos (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos_pacientes ADD CONSTRAINT fk_e740bf0366bcbb32 FOREIGN KEY (pacientes_id) REFERENCES pacientes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP CONSTRAINT FK_91E489507310DAD4');
        $this->addSql('DROP INDEX IDX_91E489507310DAD4');
        $this->addSql('ALTER TABLE cuadros_clinicos DROP paciente_id');
    }
}
