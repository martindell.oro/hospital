<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409202918 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE enfermeria_guardia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE enfermeria_guardia (id INT NOT NULL, usuario_id_id INT NOT NULL, valoraciones_id INT DEFAULT NULL, fecha DATE DEFAULT NULL, hora TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, observaciones VARCHAR(3000) DEFAULT NULL, via VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_42C18A93629AF449 ON enfermeria_guardia (usuario_id_id)');
        $this->addSql('CREATE INDEX IDX_42C18A93E051C8FC ON enfermeria_guardia (valoraciones_id)');
        $this->addSql('ALTER TABLE enfermeria_guardia ADD CONSTRAINT FK_42C18A93629AF449 FOREIGN KEY (usuario_id_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE enfermeria_guardia ADD CONSTRAINT FK_42C18A93E051C8FC FOREIGN KEY (valoraciones_id) REFERENCES valoraciones (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE enfermeria_guardia_id_seq CASCADE');
        $this->addSql('DROP TABLE enfermeria_guardia');
    }
}
