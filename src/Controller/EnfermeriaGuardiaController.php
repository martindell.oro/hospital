<?php

namespace App\Controller;

use App\Entity\EnfermeriaGuardia;
use App\Form\EnfermeriaGuardiaType;
use App\Repository\EnfermeriaGuardiaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ValoracionesRepository;


/**
 * @Route("/enfermeriaguardia")
 */
class EnfermeriaGuardiaController extends AbstractController
{
    /**
     * @Route("/", name="enfermeria_guardia_index", methods={"GET"})
     */
    public function index(EnfermeriaGuardiaRepository $enfermeriaGuardiaRepository): Response
    {
        return $this->render('enfermeria_guardia/index.html.twig', [
            'enfermeria_guardias' => $enfermeriaGuardiaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="enfermeria_guardia_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValoracionesRepository $valoracionesRepository): Response
    {
        $enfermeriaGuardium = new EnfermeriaGuardia();
        $valoracion = $valoracionesRepository->find($request->query->get('valoracion'));
        $form = $this->createForm(EnfermeriaGuardiaType::class, $enfermeriaGuardium);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $enfermeriaGuardium->setValoraciones($valoracion);
            $enfermeriaGuardium->setUsuarioId($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enfermeriaGuardium);
            $entityManager->flush();

            return $this->redirectToRoute('valoraciones_show',
              [
                'id' => $enfermeriaGuardium->getValoraciones()->getId()
              ]
            );
        }

        return $this->render('enfermeria_guardia/new.html.twig', [
            'enfermeria_guardium' => $enfermeriaGuardium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="enfermeria_guardia_show", methods={"GET"})
     */
    public function show(EnfermeriaGuardia $enfermeriaGuardium): Response
    {
        return $this->render('enfermeria_guardia/show.html.twig', [
            'enfermeria_guardium' => $enfermeriaGuardium,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="enfermeria_guardia_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EnfermeriaGuardia $enfermeriaGuardium): Response
    {
        $form = $this->createForm(EnfermeriaGuardiaType::class, $enfermeriaGuardium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('enfermeria_guardia_index');
        }

        return $this->render('enfermeria_guardia/edit.html.twig', [
            'enfermeria_guardium' => $enfermeriaGuardium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="enfermeria_guardia_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EnfermeriaGuardia $enfermeriaGuardium): Response
    {
        if ($this->isCsrfTokenValid('delete'.$enfermeriaGuardium->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($enfermeriaGuardium);
            $entityManager->flush();
        }

        return $this->redirectToRoute('enfermeria_guardia_index');
    }
}
