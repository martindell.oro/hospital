<?php

namespace App\Controller;

use App\Entity\Pacientes;
use App\Form\PacientesType;
use App\Repository\PacientesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pacientes")
 */
class PacientesController extends AbstractController
{
    /**
     * @Route("/", name="pacientes_index", methods={"GET"})
     */
    public function index(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $search = $request->get('search');
        $dni = null;
        if ($search) {
          if ((int)$search != 0) {
            $repository = $pacientesRepository->findByDni($search);
          } else {
            $repository = $pacientesRepository->findByApellido($search);
          }
          return $this->render('pacientes/index.html.twig', [
              'pacientes' => $repository,
          ]);
        }
        return $this->render('pacientes/index.html.twig', [
            'pacientes' => $pacientesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pacientes_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $paciente = new Pacientes();
        $form = $this->createForm(PacientesType::class, $paciente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($paciente);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add('sucess', 'Se ha creado un nuevo Paciente');

            return $this->redirectToRoute('triage_paciente_new', ['paciente' => $paciente->getId()]);
          }

          catch(DBALException $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
          }
          catch(\Exception $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
          }
        }

        return $this->render('pacientes/new.html.twig', [
            'paciente' => $paciente,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pacientes_show", methods={"GET"})
     */
    public function show(Pacientes $paciente): Response
    {
        return $this->render('pacientes/show.html.twig', [
            'paciente' => $paciente,
        ]);
    }

    /**
     * @Route("/{id}/cuadroclinico", name="cuadros_clinicos_show_paciente", methods={"GET"})
     */
    public function showCuadroClinico(Pacientes $paciente): Response
    {
        return $this->render('pacientes/show_cuadro_clinico.html.twig', [
            'paciente' => $paciente,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pacientes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pacientes $paciente): Response
    {
        $form = $this->createForm(PacientesType::class, $paciente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pacientes_index');
        }

        return $this->render('pacientes/edit.html.twig', [
            'paciente' => $paciente,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pacientes_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pacientes $paciente): Response
    {
        if ($this->isCsrfTokenValid('delete'.$paciente->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($paciente);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pacientes_index');
    }
}
