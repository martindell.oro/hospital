<?php

namespace App\Controller;

use App\Entity\Reservas;
use App\Form\ReservasType;
use App\Repository\ReservasRepository;
use App\Repository\HospedajesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PacientesRepository;

/**
 * @Route("/reservas")
 */
class ReservasController extends AbstractController
{
    /**
     * @Route("/", name="reservas_index", methods={"GET"})
     */
    public function index(ReservasRepository $reservasRepository): Response
    {
        return $this->render('reservas/index.html.twig', [
            'reservas' => $reservasRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ocupacion", name="ocupacion", methods={"GET"})
     */
    public function getOcupacion(HospedajesRepository $hospedajesRepository): Response
    {
        $disponibilidad = $hospedajesRepository->ocupacionActual();
        foreach ($disponibilidad as $key => $value) {
          $disponibilidad[$key]['disponible']=$disponibilidad[$key]['capacidadMaxima']-$disponibilidad[$key]['ocupado'];
          $disponibilidad[$key]['respiradoresDisponibles']=$disponibilidad[$key]['cantidadRespiradores']-$disponibilidad[$key]['respiradoresOcupados'];
        }
        $institucionHospitalaria = array_filter($disponibilidad, function($var){
          if ($var['institucionHospitalaria']==true){
            return $var;
          }
        });
        $institucionExtraHospitalaria = array_filter($disponibilidad, function($var){
          if ($var['institucionHospitalaria']==false || $var['institucionHospitalaria']==null){
            return $var;
          }
        });
        $result = array('institucionHospitalaria'=>$institucionHospitalaria, 'institucionExtraHospitalaria'=> $institucionExtraHospitalaria);
        return $result;
    }

    /**
     * @Route("/ocupacion.json", name="ocupacion", methods={"GET"})
     */
    public function getOcupacionJson(HospedajesRepository $hospedajesRepository): Response
    {
        $disponibilidad = $hospedajesRepository->ocupacionActual();
        foreach ($disponibilidad as $key => $value) {
          $disponibilidad[$key]['disponible']=$disponibilidad[$key]['capacidadMaxima']-$disponibilidad[$key]['ocupado'];
          $disponibilidad[$key]['respiradoresDisponibles']=$disponibilidad[$key]['cantidadRespiradores']-$disponibilidad[$key]['respiradoresOcupados'];
        }
        $institucionHospitalaria = array_filter($disponibilidad, function($var){
          if ($var['institucionHospitalaria']==true){
            return $var;
          }
        });
        $institucionExtraHospitalaria = array_filter($disponibilidad, function($var){
          if ($var['institucionHospitalaria']==false || $var['institucionHospitalaria']==null){
            return $var;
          }
        });
        $result = array('institucionHospitalaria'=>$institucionHospitalaria, 'institucionExtraHospitalaria'=> $institucionExtraHospitalaria);
        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="reservas_new", methods={"GET","POST"})
     */
    public function new(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        $reserva = new Reservas();
        $form = $this->createForm(ReservasType::class, $reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reserva->setPaciente($paciente);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reserva);
            $entityManager->flush();

            return $this->redirectToRoute('pacientes_show',['id' => $paciente->getId()]);
        }

        return $this->render('reservas/new.html.twig', [
            'reserva' => $reserva,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reservas_show", methods={"GET"})
     */
    public function show(Reservas $reserva): Response
    {
        return $this->render('reservas/show.html.twig', [
            'reserva' => $reserva,
        ]);
    }

    /**
     * @Route("/{id}/checkout", name="checkout", methods={"GET","POST"})
     */
    public function checkout(Request $request, Reservas $reserva): Response
    {
        $today = new \DateTime();
        $reserva->setFechaHasta($today);
        $reserva->setCheckout(true);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('pacientes_show',['id' => $reserva->getPaciente()->getId()]);
    }

    /**
     * @Route("/{id}/edit", name="reservas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Reservas $reserva): Response
    {
        $form = $this->createForm(ReservasType::class, $reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reservas_index');
        }

        return $this->render('reservas/edit.html.twig', [
            'reserva' => $reserva,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reservas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Reservas $reserva): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reserva->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reserva);
            $entityManager->flush();
        }

        return $this->redirectToRoute('reservas_index');
    }
}
