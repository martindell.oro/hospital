<?php

namespace App\Controller;

use App\Entity\HistorialKardecs;
use App\Form\HistorialKardecsType;
use App\Repository\HistorialKardecsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\KardecsItemsRepository;
/**
 * @Route("/historial/kardecs")
 */
class HistorialKardecsController extends AbstractController
{
    /**
     * @Route("/", name="historial_kardecs_index", methods={"GET"})
     */
    public function index(HistorialKardecsRepository $historialKardecsRepository): Response
    {
        return $this->render('historial_kardecs/index.html.twig', [
            'historial_kardecs' => $historialKardecsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="historial_kardecs_new", methods={"GET","POST"})
     */
    public function new(Request $request, KardecsItemsRepository $kardecsItemRepository): Response
    {
        $kardecItem = $kardecsItemRepository->find($request->query->get('kardecItem'));
        $historialKardec = new HistorialKardecs();
        $form = $this->createForm(HistorialKardecsType::class, $historialKardec);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $historialKardec->setKardecItem($kardecItem);
            $historialKardec->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($historialKardec);
            $entityManager->flush();
            return $this->redirectToRoute('kardecs_show', ['id' => $historialKardec->getKardecItem()->getKardec()->getId()]);
        }

        return $this->render('historial_kardecs/new.html.twig', [
            'historial_kardec' => $historialKardec,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="historial_kardecs_show", methods={"GET"})
     */
    public function show(HistorialKardecs $historialKardec): Response
    {
        return $this->render('historial_kardecs/show.html.twig', [
            'historial_kardec' => $historialKardec,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="historial_kardecs_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HistorialKardecs $historialKardec): Response
    {
        $form = $this->createForm(HistorialKardecsType::class, $historialKardec);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('historial_kardecs_index');
        }

        return $this->render('historial_kardecs/edit.html.twig', [
            'historial_kardec' => $historialKardec,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="historial_kardecs_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HistorialKardecs $historialKardec): Response
    {
        if ($this->isCsrfTokenValid('delete'.$historialKardec->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($historialKardec);
            $entityManager->flush();
        }

        return $this->redirectToRoute('historial_kardecs_index');
    }
}
