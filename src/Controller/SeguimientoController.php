<?php

namespace App\Controller;

use App\Entity\Seguimiento;
use App\Form\SeguimientoType;
use App\Repository\SeguimientoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/seguimiento")
 */
class SeguimientoController extends AbstractController
{
    /**
     * @Route("/", name="seguimiento_index", methods={"GET"})
     */
    public function index(Request $request, SeguimientoRepository $seguimientoRepository): Response
    {
        $search = $request->get('search');
        $dni = null;
        if ($search) {
          if ((int)$search != 0) {
            $repository = $seguimientoRepository->findByDni($search);
          } else {
            $repository = $seguimientoRepository->findByApellido($search);
          }
          return $this->render('seguimiento/index.html.twig', [
              'seguimientos' => $repository,
          ]);
        }
        return $this->render('seguimiento/index.html.twig', [
            'seguimientos' => $seguimientoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="seguimiento_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $seguimiento = new Seguimiento();
        $form = $this->createForm(SeguimientoType::class, $seguimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $seguimiento->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seguimiento);
            $entityManager->flush();

            return $this->redirectToRoute('seguimiento_show', array('id' => $seguimiento->getId()));
        }

        return $this->render('seguimiento/new.html.twig', [
            'seguimiento' => $seguimiento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seguimiento_show", methods={"GET"})
     */
    public function show(Seguimiento $seguimiento): Response
    {
        return $this->render('seguimiento/show.html.twig', [
            'seguimiento' => $seguimiento,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seguimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Seguimiento $seguimiento): Response
    {
        $form = $this->createForm(SeguimientoType::class, $seguimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('seguimiento_show', array('id' => $seguimiento->getId()));
        }

        return $this->render('seguimiento/edit.html.twig', [
            'seguimiento' => $seguimiento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seguimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Seguimiento $seguimiento): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seguimiento->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seguimiento);
            $entityManager->flush();
        }

        return $this->redirectToRoute('seguimiento_index');
    }
}
