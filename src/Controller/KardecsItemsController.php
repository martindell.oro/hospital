<?php

namespace App\Controller;

use App\Entity\KardecsItems;
use App\Form\KardecsItemsType;
use App\Repository\KardecsItemsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\KardecsRepository;

/**
 * @Route("/kardecs/items")
 */
class KardecsItemsController extends AbstractController
{
    /**
     * @Route("/", name="kardecs_items_index", methods={"GET"})
     */
    public function index(KardecsItemsRepository $kardecsItemsRepository): Response
    {
        return $this->render('kardecs_items/index.html.twig', [
            'kardecs_items' => $kardecsItemsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="kardecs_items_new", methods={"GET","POST"})
     */
    public function new(Request $request, KardecsRepository $kardecsRepository): Response
    {
        $kardec = $kardecsRepository->find($request->query->get('kardec'));
        $kardecsItem = new KardecsItems();
        $form = $this->createForm(KardecsItemsType::class, $kardecsItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $kardecsItem->setKardec($kardec);
            $kardecsItem->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($kardecsItem);
            $entityManager->flush();
            return $this->redirectToRoute('kardecs_show', ['id' => $kardecsItem->getKardec()->getId()]);
        }

        return $this->render('kardecs_items/new.html.twig', [
            'kardecs_item' => $kardecsItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="kardecs_items_show", methods={"GET"})
     */
    public function show(KardecsItems $kardecsItem): Response
    {
        return $this->render('kardecs_items/show.html.twig', [
            'kardecs_item' => $kardecsItem,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="kardecs_items_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, KardecsItems $kardecsItem): Response
    {
        $form = $this->createForm(KardecsItemsType::class, $kardecsItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('kardecs_items_index');
        }

        return $this->render('kardecs_items/edit.html.twig', [
            'kardecs_item' => $kardecsItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="kardecs_items_delete", methods={"DELETE"})
     */
    public function delete(Request $request, KardecsItems $kardecsItem): Response
    {
        if ($this->isCsrfTokenValid('delete'.$kardecsItem->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($kardecsItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('kardecs_items_index');
    }
}
