<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\HospedajesRepository;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(Request $request, HospedajesRepository $hospedajesRepository): Response
    {
        if ($this->getUser()) {
          $hospedajes = $this->forward('App\Controller\ReservasController::getOcupacionJson');
          $hospedajes =json_decode($hospedajes->getContent(),true);
          return $this->render('default/index.html.twig', array('hospedajes'=>$hospedajes));
        }
        return $this->redirectToRoute('fos_user_security_login');
    }
}
