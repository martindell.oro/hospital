<?php

namespace App\Controller;

use App\Entity\Sintomas;
use App\Form\SintomasType;
use App\Repository\SintomasRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sintomas")
 */
class SintomasController extends AbstractController
{
    /**
     * @Route("/", name="sintomas_index", methods={"GET"})
     */
    public function index(SintomasRepository $sintomasRepository): Response
    {
        return $this->render('sintomas/index.html.twig', [
            'sintomas' => $sintomasRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sintomas_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sintoma = new Sintomas();
        $form = $this->createForm(SintomasType::class, $sintoma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sintoma);
            $entityManager->flush();

            return $this->redirectToRoute('sintomas_index');
        }

        return $this->render('sintomas/new.html.twig', [
            'sintoma' => $sintoma,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sintomas_show", methods={"GET"})
     */
    public function show(Sintomas $sintoma): Response
    {
        return $this->render('sintomas/show.html.twig', [
            'sintoma' => $sintoma,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sintomas_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sintomas $sintoma): Response
    {
        $form = $this->createForm(SintomasType::class, $sintoma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sintomas_index');
        }

        return $this->render('sintomas/edit.html.twig', [
            'sintoma' => $sintoma,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sintomas_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sintomas $sintoma): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sintoma->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sintoma);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sintomas_index');
    }
}
