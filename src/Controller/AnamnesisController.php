<?php

namespace App\Controller;

use App\Entity\Anamnesis;
use App\Form\AnamnesisType;
use App\Repository\AnamnesisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PacientesRepository;

/**
 * @Route("/anamnesis")
 */
class AnamnesisController extends AbstractController
{
    /**
     * @Route("/", name="anamnesis_index", methods={"GET"})
     */
    public function index(AnamnesisRepository $anamnesisRepository): Response
    {
        return $this->render('anamnesis/index.html.twig', [
            'anamneses' => $anamnesisRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="anamnesis_new", methods={"GET","POST"})
     */
    public function new(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        if(in_array('ROLE_MEDICO', $this->getUser()->getRoles())){
          $anamnesi = new Anamnesis();
          $form = $this->createForm(AnamnesisType::class, $anamnesi);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
              $anamnesi->setPaciente($paciente);
              $anamnesi->setUsuario($this->getUser());
              $entityManager = $this->getDoctrine()->getManager();
              $entityManager->persist($anamnesi);
              $entityManager->flush();

              return $this->redirectToRoute('pacientes_show', ['id' => $paciente->getId()]);
          }

          return $this->render('anamnesis/new.html.twig', [
              'anamnesi' => $anamnesi,
              'form' => $form->createView(),
          ]);
        }
        return $this->redirectToRoute('pacientes_show', ['id' => $paciente->getId()]);
    }

    /**
     * @Route("/{id}", name="anamnesis_show", methods={"GET"})
     */
    public function show(Anamnesis $anamnesi): Response
    {
        return $this->render('anamnesis/show.html.twig', [
            'anamnesi' => $anamnesi,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="anamnesis_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Anamnesis $anamnesi): Response
    {
      if(in_array('ROLE_MEDICO', $this->getUser()->getRoles())){
        $form = $this->createForm(AnamnesisType::class, $anamnesi);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('pacientes_show', ['id' => $anamnesi->getPaciente()->getId()]);
        }

        return $this->render('anamnesis/edit.html.twig', [
            'anamnesi' => $anamnesi,
            'form' => $form->createView(),
        ]);
      }
      return $this->redirectToRoute('pacientes_show', ['id' => $anamnesi->getPaciente()->getId()]);
    }

    /**
     * @Route("/{id}", name="anamnesis_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Anamnesis $anamnesi): Response
    {
        if ($this->isCsrfTokenValid('delete'.$anamnesi->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($anamnesi);
            $entityManager->flush();
        }

        return $this->redirectToRoute('anamnesis_index');
    }
}
