<?php

namespace App\Controller;

use App\Entity\Kardecs;
use App\Form\KardecsType;
use App\Repository\KardecsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PacientesRepository;

/**
 * @Route("/kardecs")
 */
class KardecsController extends AbstractController
{
    /**
     * @Route("/", name="kardecs_index", methods={"GET"})
     */
    public function index(KardecsRepository $kardecsRepository): Response
    {
        return $this->render('kardecs/index.html.twig', [
            'kardecs' => $kardecsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="kardecs_new", methods={"GET","POST"})
     */
    public function new(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        $kardec = new Kardecs();
        $form = $this->createForm(KardecsType::class, $kardec);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $kardec->setPaciente($paciente);
            $kardec->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($kardec);
            $entityManager->flush();

            return $this->redirectToRoute('kardecs_show', ['id' => $kardec->getId()]);
        }

        return $this->render('kardecs/new.html.twig', [
            'kardec' => $kardec,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="kardecs_show", methods={"GET"})
     */
    public function show(Kardecs $kardec): Response
    {
        return $this->render('kardecs/show.html.twig', [
            'kardec' => $kardec,
        ]);
    }

    // /**
    //  * @Route("/{id}/edit", name="kardecs_edit", methods={"GET","POST"})
    //  */
    // public function edit(Request $request, Kardecs $kardec): Response
    // {
    //     $form = $this->createForm(KardecsType::class, $kardec);
    //     $form->handleRequest($request);
    //
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->getDoctrine()->getManager()->flush();
    //
    //         return $this->redirectToRoute('kardecs_index');
    //     }
    //
    //     return $this->render('kardecs/edit.html.twig', [
    //         'kardec' => $kardec,
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * @Route("/{id}", name="kardecs_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Kardecs $kardec): Response
    {
        if ($this->isCsrfTokenValid('delete'.$kardec->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($kardec);
            $entityManager->flush();
        }

        return $this->redirectToRoute('kardecs_index');
    }
}
