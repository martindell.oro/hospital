<?php

namespace App\Controller;

use App\Entity\CuadrosClinicosItem;
use App\Form\CuadrosClinicosItemType;
use App\Repository\CuadrosClinicosItemRepository;
use App\Repository\CuadrosClinicosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cuadros_clinicos_item")
 */
class CuadrosClinicosItemController extends AbstractController
{
    /**
     * @Route("/", name="cuadros_clinicos_item_index", methods={"GET"})
     */
    public function index(CuadrosClinicosItemRepository $cuadrosClinicosItemRepository): Response
    {
        return $this->render('cuadros_clinicos_item/index.html.twig', [
            'cuadros_clinicos_items' => $cuadrosClinicosItemRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="cuadros_clinicos_item_new", methods={"GET","POST"})
     */
    public function new(Request $request, CuadrosClinicosRepository $cuadrosClinicosRepository): Response
    {
        $cuadroClinico = $cuadrosClinicosRepository->find($request->query->get('cuadroClinico'));
        $cuadrosClinicosItem = new CuadrosClinicosItem();
        $form = $this->createForm(CuadrosClinicosItemType::class, $cuadrosClinicosItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          try {
            $cuadrosClinicosItem->setUsuario($this->getUser());
            $cuadrosClinicosItem->setCuadrosClinicos($cuadroClinico);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cuadrosClinicosItem);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add('sucess', 'Se ha creado un nuevo item en el Cuadro Clínico');
            return $this->redirectToRoute('cuadros_clinicos_show', array('id'=>$cuadroClinico->getId()));
          }
          catch(DBALException $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
          }
          catch(\Exception $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
            return $this->redirectToRoute('cuadros_clinicos_show', array('id'=>$cuadroClinico->getId()));
          }
        }

        return $this->render('cuadros_clinicos_item/new.html.twig', [
            'cuadros_clinicos_item' => $cuadrosClinicosItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cuadros_clinicos_item_show", methods={"GET"})
     */
    public function show(CuadrosClinicosItem $cuadrosClinicosItem): Response
    {
        return $this->render('cuadros_clinicos_item/show.html.twig', [
            'cuadros_clinicos_item' => $cuadrosClinicosItem,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cuadros_clinicos_item_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CuadrosClinicosItem $cuadrosClinicosItem): Response
    {
        $form = $this->createForm(CuadrosClinicosItemType::class, $cuadrosClinicosItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cuadros_clinicos_item_index');
        }

        return $this->render('cuadros_clinicos_item/edit.html.twig', [
            'cuadros_clinicos_item' => $cuadrosClinicosItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cuadros_clinicos_item_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CuadrosClinicosItem $cuadrosClinicosItem): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cuadrosClinicosItem->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cuadrosClinicosItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cuadros_clinicos_item_index');
    }
}
