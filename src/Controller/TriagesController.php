<?php

namespace App\Controller;

use App\Entity\Triages;
use App\Form\TriagesType;
use App\Repository\TriagesRepository;
use App\Repository\PacientesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/triages")
 */
class TriagesController extends AbstractController
{
    /**
     * @Route("/", name="triages_index", methods={"GET"})
     */
    public function index(TriagesRepository $triagesRepository): Response
    {
        return $this->render('triages/index.html.twig', [
            'triages' => $triagesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="triages_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $triage = new Triages();
        $form = $this->createForm(TriagesType::class, $triage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($triage);
            $entityManager->flush();

            return $this->redirectToRoute('triages_index');
        }

        return $this->render('triages/new.html.twig', [
            'triage' => $triage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newPaciente", name="triage_paciente_new", methods={"GET","POST"})
     */
    public function newPaciente(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        $triage = new Triages();
        $form = $this->createForm(TriagesType::class, $triage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          try {
            $triage->setPaciente($paciente);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($triage);
            $entityManager->flush();
            return $this->redirectToRoute('pacientes_show', array('id'=>$paciente->getId()));
          }
          catch(DBALException $e){
            $this->get('session')->getFlashBag()->add('error', 'Your custom message');
            $errorMessage = $e->getMessage();
          }
          catch(\Exception $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
            return $this->redirectToRoute('pacientes_show', array('id'=>$paciente->getId()));
          }
        }

        return $this->render('triages/new.html.twig', [
            'triage' => $triage,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="triages_show", methods={"GET"})
     */
    public function show(Triages $triage): Response
    {
        return $this->render('triages/show.html.twig', [
            'triage' => $triage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="triages_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Triages $triage): Response
    {
        $form = $this->createForm(TriagesType::class, $triage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pacientes_index');
        }

        return $this->render('triages/edit.html.twig', [
            'triage' => $triage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="triages_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Triages $triage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$triage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($triage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('triages_index');
    }
}
