<?php

namespace App\Controller;

use App\Entity\PrescripcionesOrdenes;
use App\Form\PrescripcionesOrdenesType;
use App\Repository\PrescripcionesOrdenesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PacientesRepository;
/**
 * @Route("/prescripciones_ordenes")
 */
class PrescripcionesOrdenesController extends AbstractController
{
    /**
     * @Route("/", name="prescripciones_ordenes_index", methods={"GET"})
     */
    public function index(PrescripcionesOrdenesRepository $prescripcionesOrdenesRepository): Response
    {
        return $this->render('prescripciones_ordenes/index.html.twig', [
            'prescripciones_ordenes' => $prescripcionesOrdenesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="prescripciones_ordenes_new", methods={"GET","POST"})
     */
    public function new(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        if(in_array('ROLE_MEDICO', $this->getUser()->getRoles())){
          $prescripcionesOrdene = new PrescripcionesOrdenes();
          $form = $this->createForm(PrescripcionesOrdenesType::class, $prescripcionesOrdene);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
              $prescripcionesOrdene->setPaciente($paciente);
              $prescripcionesOrdene->setUsuario($this->getUser());
              $entityManager = $this->getDoctrine()->getManager();
              $entityManager->persist($prescripcionesOrdene);
              $entityManager->flush();
              return $this->redirectToRoute('pacientes_show', ['id' => $paciente->getId()]);
          }

          return $this->render('prescripciones_ordenes/new.html.twig', [
              'prescripciones_ordene' => $prescripcionesOrdene,
              'form' => $form->createView(),
          ]);
        }
        return $this->redirectToRoute('pacientes_show', ['id' => $paciente->getId()]);
    }

    /**
     * @Route("/{id}", name="prescripciones_ordenes_show", methods={"GET"})
     */
    public function show(PrescripcionesOrdenes $prescripcionesOrdene): Response
    {
        return $this->render('prescripciones_ordenes/show.html.twig', [
            'prescripciones_ordene' => $prescripcionesOrdene,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="prescripciones_ordenes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PrescripcionesOrdenes $prescripcionesOrdene): Response
    {
        if(in_array('ROLE_MEDICO', $this->getUser()->getRoles())){
          $form = $this->createForm(PrescripcionesOrdenesType::class, $prescripcionesOrdene);
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {
              $this->getDoctrine()->getManager()->flush();

              return $this->redirectToRoute('prescripciones_ordenes_index');
          }

          return $this->render('prescripciones_ordenes/edit.html.twig', [
              'prescripciones_ordene' => $prescripcionesOrdene,
              'form' => $form->createView(),
          ]);
        }
      return $this->redirectToRoute('pacientes_show', ['id' => $prescripcionesOrdene->getPaciente()->getId()]);

    }

    /**
     * @Route("/{id}", name="prescripciones_ordenes_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PrescripcionesOrdenes $prescripcionesOrdene): Response
    {
        if ($this->isCsrfTokenValid('delete'.$prescripcionesOrdene->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($prescripcionesOrdene);
            $entityManager->flush();
        }

        return $this->redirectToRoute('prescripciones_ordenes_index');
    }
}
