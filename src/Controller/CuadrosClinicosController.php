<?php

namespace App\Controller;

use App\Entity\CuadrosClinicos;
use App\Form\CuadrosClinicosType;
use App\Repository\CuadrosClinicosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PacientesRepository;

/**
 * @Route("/cuadros_clinicos")
 */
class CuadrosClinicosController extends AbstractController
{
    /**
     * @Route("/", name="cuadros_clinicos_index", methods={"GET"})
     */
    public function index(CuadrosClinicosRepository $cuadrosClinicosRepository): Response
    {
        return $this->render('cuadros_clinicos/index.html.twig', [
            'cuadros_clinicos' => $cuadrosClinicosRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="cuadros_clinicos_new", methods={"GET","POST"})
     */
    public function new(Request $request, PacientesRepository $pacientesRepository): Response
    {
        $paciente = $pacientesRepository->find($request->query->get('paciente'));
        $cuadrosClinico = new CuadrosClinicos();
        $form = $this->createForm(CuadrosClinicosType::class, $cuadrosClinico);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          try {
            $cuadrosClinico->setPaciente($paciente);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cuadrosClinico);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add('sucess', 'Se ha creado un nuevo Cuadro Clínico');
            return $this->redirectToRoute('cuadros_clinicos_show', array('id'=>$cuadrosClinico->getId()));
          }
          catch(DBALException $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
          }
          catch(\Exception $e){
            $this->get('session')->getFlashBag()->add('error', 'Ocurrió un error');
            $errorMessage = $e->getMessage();
            return $this->redirectToRoute('pacientes_show', array('id'=>$paciente->getId()));
          }
        }

        return $this->render('cuadros_clinicos/new.html.twig', [
            'cuadros_clinico' => $cuadrosClinico,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cuadros_clinicos_show", methods={"GET"})
     */
    public function show(CuadrosClinicos $cuadrosClinico): Response
    {
        return $this->render('cuadros_clinicos/show.html.twig', [
            'cuadros_clinico' => $cuadrosClinico,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cuadros_clinicos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CuadrosClinicos $cuadrosClinico): Response
    {
        $form = $this->createForm(CuadrosClinicosType::class, $cuadrosClinico);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cuadros_clinicos_index');
        }

        return $this->render('cuadros_clinicos/edit.html.twig', [
            'cuadros_clinico' => $cuadrosClinico,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cuadros_clinicos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CuadrosClinicos $cuadrosClinico): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cuadrosClinico->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cuadrosClinico);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cuadros_clinicos_index');
    }
}
