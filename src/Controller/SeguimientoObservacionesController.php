<?php

namespace App\Controller;

use App\Entity\SeguimientoObservaciones;
use App\Form\SeguimientoObservacionesType;
use App\Repository\SeguimientoObservacionesRepository;
use App\Repository\SeguimientoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/seguimiento/observaciones")
 */
class SeguimientoObservacionesController extends AbstractController
{
    /**
     * @Route("/", name="seguimiento_observaciones_index", methods={"GET"})
     */
    public function index(SeguimientoObservacionesRepository $seguimientoObservacionesRepository): Response
    {
        return $this->render('seguimiento_observaciones/index.html.twig', [
            'seguimiento_observaciones' => $seguimientoObservacionesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="seguimiento_observaciones_new", methods={"GET","POST"})
     */
    public function new(Request $request, SeguimientoRepository $seguimientoRepository): Response
    {
        $seguimiento = $seguimientoRepository->find($request->query->get('seg'));
        $seguimientoObservacione = new SeguimientoObservaciones();
        $form = $this->createForm(SeguimientoObservacionesType::class, $seguimientoObservacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $seguimientoObservacione->setSeguimiento($seguimiento);
            $seguimientoObservacione->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seguimientoObservacione);
            $entityManager->flush();

            return $this->redirectToRoute('seguimiento_show', array('id' => $seguimiento->getId()));
        }

        return $this->render('seguimiento_observaciones/new.html.twig', [
            'seguimiento_observacione' => $seguimientoObservacione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seguimiento_observaciones_show", methods={"GET"})
     */
    public function show(SeguimientoObservaciones $seguimientoObservacione): Response
    {
        return $this->render('seguimiento_observaciones/show.html.twig', [
            'seguimiento_observacione' => $seguimientoObservacione,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seguimiento_observaciones_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SeguimientoObservaciones $seguimientoObservacione): Response
    {
        $form = $this->createForm(SeguimientoObservacionesType::class, $seguimientoObservacione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seguimiento_observaciones_index');
        }

        return $this->render('seguimiento_observaciones/edit.html.twig', [
            'seguimiento_observacione' => $seguimientoObservacione,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seguimiento_observaciones_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SeguimientoObservaciones $seguimientoObservacione): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seguimientoObservacione->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seguimientoObservacione);
            $entityManager->flush();
        }

        return $this->redirectToRoute('seguimiento_observaciones_index');
    }
}
