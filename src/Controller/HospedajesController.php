<?php

namespace App\Controller;

use App\Entity\Hospedajes;
use App\Form\HospedajesType;
use App\Repository\HospedajesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hospedajes")
 */
class HospedajesController extends AbstractController
{
    /**
     * @Route("/", name="hospedajes_index", methods={"GET"})
     */
    public function index(HospedajesRepository $hospedajesRepository): Response
    {
        return $this->render('hospedajes/index.html.twig', [
            'hospedajes' => $hospedajesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="hospedajes_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $hospedaje = new Hospedajes();
        $form = $this->createForm(HospedajesType::class, $hospedaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($hospedaje);
            $entityManager->flush();

            return $this->redirectToRoute('hospedajes_index');
        }

        return $this->render('hospedajes/new.html.twig', [
            'hospedaje' => $hospedaje,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="hospedajes_show", methods={"GET"})
     */
    public function show(Hospedajes $hospedaje): Response
    {
        $pacientesActivos = array();
        foreach ($hospedaje->getReservas() as $key => $value) {
          if ($value->getCheckout() <> true) {
            $pacientesActivos[$key]=$value;
          }
        }
        return $this->render('hospedajes/show.html.twig', [
            'hospedaje' => $hospedaje,
            'pacientesActivos' =>$pacientesActivos
        ]);
    }

    /**
     * @Route("/{id}/edit", name="hospedajes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Hospedajes $hospedaje): Response
    {
        $form = $this->createForm(HospedajesType::class, $hospedaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('hospedajes_index');
        }

        return $this->render('hospedajes/edit.html.twig', [
            'hospedaje' => $hospedaje,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="hospedajes_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Hospedajes $hospedaje): Response
    {
        if ($this->isCsrfTokenValid('delete'.$hospedaje->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($hospedaje);
            $entityManager->flush();
        }

        return $this->redirectToRoute('hospedajes_index');
    }
}
