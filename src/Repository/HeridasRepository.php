<?php

namespace App\Repository;

use App\Entity\Heridas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Heridas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Heridas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Heridas[]    findAll()
 * @method Heridas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeridasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Heridas::class);
    }

    // /**
    //  * @return Heridas[] Returns an array of Heridas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Heridas
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
