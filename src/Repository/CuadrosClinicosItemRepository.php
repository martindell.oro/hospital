<?php

namespace App\Repository;

use App\Entity\CuadrosClinicosItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CuadrosClinicosItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method CuadrosClinicosItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method CuadrosClinicosItem[]    findAll()
 * @method CuadrosClinicosItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CuadrosClinicosItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CuadrosClinicosItem::class);
    }

    // /**
    //  * @return CuadrosClinicosItem[] Returns an array of CuadrosClinicosItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CuadrosClinicosItem
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
