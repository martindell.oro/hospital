<?php

namespace App\Repository;

use App\Entity\Sintomas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Sintomas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sintomas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sintomas[]    findAll()
 * @method Sintomas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SintomasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sintomas::class);
    }

    // /**
    //  * @return Sintomas[] Returns an array of Sintomas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sintomas
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
