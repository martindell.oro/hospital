<?php

namespace App\Repository;

use App\Entity\EliminacionInstestinal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EliminacionInstestinal|null find($id, $lockMode = null, $lockVersion = null)
 * @method EliminacionInstestinal|null findOneBy(array $criteria, array $orderBy = null)
 * @method EliminacionInstestinal[]    findAll()
 * @method EliminacionInstestinal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EliminacionInstestinalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EliminacionInstestinal::class);
    }

    // /**
    //  * @return EliminacionInstestinal[] Returns an array of EliminacionInstestinal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EliminacionInstestinal
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
