<?php

namespace App\Repository;

use App\Entity\HistorialKardecs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HistorialKardecs|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistorialKardecs|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistorialKardecs[]    findAll()
 * @method HistorialKardecs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistorialKardecsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistorialKardecs::class);
    }

    // /**
    //  * @return HistorialKardecs[] Returns an array of HistorialKardecs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HistorialKardecs
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
