<?php

namespace App\Repository;

use App\Entity\PrescripcionesOrdenes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PrescripcionesOrdenes|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrescripcionesOrdenes|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrescripcionesOrdenes[]    findAll()
 * @method PrescripcionesOrdenes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrescripcionesOrdenesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrescripcionesOrdenes::class);
    }

    // /**
    //  * @return PrescripcionesOrdenes[] Returns an array of PrescripcionesOrdenes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrescripcionesOrdenes
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
