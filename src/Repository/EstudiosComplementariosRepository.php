<?php

namespace App\Repository;

use App\Entity\EstudiosComplementarios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EstudiosComplementarios|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstudiosComplementarios|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstudiosComplementarios[]    findAll()
 * @method EstudiosComplementarios[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudiosComplementariosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstudiosComplementarios::class);
    }

    // /**
    //  * @return EstudiosComplementarios[] Returns an array of EstudiosComplementarios objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstudiosComplementarios
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
