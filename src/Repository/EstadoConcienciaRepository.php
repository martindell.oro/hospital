<?php

namespace App\Repository;

use App\Entity\EstadoConciencia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EstadoConciencia|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstadoConciencia|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstadoConciencia[]    findAll()
 * @method EstadoConciencia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstadoConcienciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstadoConciencia::class);
    }

    // /**
    //  * @return EstadoConciencia[] Returns an array of EstadoConciencia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstadoConciencia
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
