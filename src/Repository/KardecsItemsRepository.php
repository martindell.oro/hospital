<?php

namespace App\Repository;

use App\Entity\KardecsItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method KardecsItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method KardecsItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method KardecsItems[]    findAll()
 * @method KardecsItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KardecsItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KardecsItems::class);
    }

    // /**
    //  * @return KardecsItems[] Returns an array of KardecsItems objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KardecsItems
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
