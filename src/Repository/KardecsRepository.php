<?php

namespace App\Repository;

use App\Entity\Kardecs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Kardecs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kardecs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kardecs[]    findAll()
 * @method Kardecs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KardecsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kardecs::class);
    }

    // /**
    //  * @return Kardecs[] Returns an array of Kardecs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Kardecs
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
