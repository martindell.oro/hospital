<?php

namespace App\Repository;

use App\Entity\ObservacionesSintomas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObservacionesSintomas|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObservacionesSintomas|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObservacionesSintomas[]    findAll()
 * @method ObservacionesSintomas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObservacionesSintomasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObservacionesSintomas::class);
    }

    // /**
    //  * @return ObservacionesSintomas[] Returns an array of ObservacionesSintomas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObservacionesSintomas
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
