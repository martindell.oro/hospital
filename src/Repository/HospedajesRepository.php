<?php

namespace App\Repository;

use App\Entity\Hospedajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Hospedajes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hospedajes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hospedajes[]    findAll()
 * @method Hospedajes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HospedajesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hospedajes::class);
    }

    /**
     * @return Reservas[] Returns an array of Reservas objects
     */

     public function findAll()
     {
         return $this->findBy(array(), array('id' => 'ASC'));
     }

    public function ocupacionActual()
    {
      return $this->createQueryBuilder('h')
          ->select('h.nombre','h.id as hospedaje', 'h.capacidadMaxima','h.cantidadRespiradores', 'h.institucionHospitalaria')
          ->addSelect('(SELECT count(r.paciente)
            FROM App\Entity\Reservas r
            WHERE h.id = r.hospedaje
            AND r.checkout is null) AS ocupado')
          ->addSelect('(SELECT count(re.respirador)
            FROM App\Entity\Reservas re
            WHERE h.id = re.hospedaje
            AND re.respirador = TRUE
            AND re.checkout is null) AS respiradoresOcupados')
          ->getQuery()
          ->getResult();
    }

    /**
     * @return Reservas[] Returns an array of Reservas objects
     */

    public function ocupacionActualTotal()
    {
      return $this->createQueryBuilder('h')
          ->select('h.capacidadMaxima - count(r.paciente) as disponible','h.nombre','h.id as hospedaje')
          ->leftJoin('App\Entity\Reservas','r','WITH','h.id = r.hospedaje')
          ->andWhere('r.checkout is null')
          ->groupBy('h.capacidadMaxima')
          ->getQuery()
          ->getResult();
    }

    // /**
    //  * @return Hospedajes[] Returns an array of Hospedajes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hospedajes
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
