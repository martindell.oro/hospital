<?php

namespace App\Repository;

use App\Entity\EliminacionUrinaria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EliminacionUrinaria|null find($id, $lockMode = null, $lockVersion = null)
 * @method EliminacionUrinaria|null findOneBy(array $criteria, array $orderBy = null)
 * @method EliminacionUrinaria[]    findAll()
 * @method EliminacionUrinaria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EliminacionUrinariaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EliminacionUrinaria::class);
    }

    // /**
    //  * @return EliminacionUrinaria[] Returns an array of EliminacionUrinaria objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EliminacionUrinaria
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
