<?php

namespace App\Repository;

use App\Entity\Triages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Triages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Triages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Triages[]    findAll()
 * @method Triages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TriagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Triages::class);
    }

    // /**
    //  * @return Triages[] Returns an array of Triages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Triages
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
