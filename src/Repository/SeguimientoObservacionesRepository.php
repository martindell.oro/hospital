<?php

namespace App\Repository;

use App\Entity\SeguimientoObservaciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SeguimientoObservaciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeguimientoObservaciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeguimientoObservaciones[]    findAll()
 * @method SeguimientoObservaciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeguimientoObservacionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeguimientoObservaciones::class);
    }

    // /**
    //  * @return SeguimientoObservaciones[] Returns an array of SeguimientoObservaciones objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SeguimientoObservaciones
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
