<?php

namespace App\Repository;

use App\Entity\AccesosPerifericos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AccesosPerifericos|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccesosPerifericos|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccesosPerifericos[]    findAll()
 * @method AccesosPerifericos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccesosPerifericosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccesosPerifericos::class);
    }

    // /**
    //  * @return AccesosPerifericos[] Returns an array of AccesosPerifericos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccesosPerifericos
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
