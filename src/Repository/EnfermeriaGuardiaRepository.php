<?php

namespace App\Repository;

use App\Entity\EnfermeriaGuardia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EnfermeriaGuardia|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnfermeriaGuardia|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnfermeriaGuardia[]    findAll()
 * @method EnfermeriaGuardia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnfermeriaGuardiaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnfermeriaGuardia::class);
    }

    // /**
    //  * @return EnfermeriaGuardia[] Returns an array of EnfermeriaGuardia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EnfermeriaGuardia
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
