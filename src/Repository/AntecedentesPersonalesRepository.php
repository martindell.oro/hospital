<?php

namespace App\Repository;

use App\Entity\AntecedentesPersonales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AntecedentesPersonales|null find($id, $lockMode = null, $lockVersion = null)
 * @method AntecedentesPersonales|null findOneBy(array $criteria, array $orderBy = null)
 * @method AntecedentesPersonales[]    findAll()
 * @method AntecedentesPersonales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AntecedentesPersonalesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AntecedentesPersonales::class);
    }

    // /**
    //  * @return AntecedentesPersonales[] Returns an array of AntecedentesPersonales objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AntecedentesPersonales
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
