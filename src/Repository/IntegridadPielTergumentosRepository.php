<?php

namespace App\Repository;

use App\Entity\IntegridadPielTergumentos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IntegridadPielTergumentos|null find($id, $lockMode = null, $lockVersion = null)
 * @method IntegridadPielTergumentos|null findOneBy(array $criteria, array $orderBy = null)
 * @method IntegridadPielTergumentos[]    findAll()
 * @method IntegridadPielTergumentos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntegridadPielTergumentosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IntegridadPielTergumentos::class);
    }

    // /**
    //  * @return IntegridadPielTergumentos[] Returns an array of IntegridadPielTergumentos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IntegridadPielTergumentos
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
