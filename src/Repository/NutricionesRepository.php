<?php

namespace App\Repository;

use App\Entity\Nutriciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Nutriciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nutriciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nutriciones[]    findAll()
 * @method Nutriciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NutricionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Nutriciones::class);
    }

    // /**
    //  * @return Nutriciones[] Returns an array of Nutriciones objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nutriciones
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
