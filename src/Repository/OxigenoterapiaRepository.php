<?php

namespace App\Repository;

use App\Entity\Oxigenoterapia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Oxigenoterapia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Oxigenoterapia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Oxigenoterapia[]    findAll()
 * @method Oxigenoterapia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OxigenoterapiaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Oxigenoterapia::class);
    }

    // /**
    //  * @return Oxigenoterapia[] Returns an array of Oxigenoterapia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Oxigenoterapia
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
